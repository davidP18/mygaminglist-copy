# MyGamingList

## Project Description
This MERN application allows users to browse through PC games, review and 'like' them, and build a collection of bookmarked, playing and completed games.

## Current Features
- Browse through games on the Home page 
- Filter games by category
- Find games from a search bar
- View specific games and data like their release date and categories
- Like/unlike games 
- View and post comments on games 
- Add/remove games from bookmarked, currently playing and completed lists
- View your current profile, like email and bio, as well as bookmarked games
- Edit personal info in your profile, including name, bio and profile picture
- View list of users and their profile details 

## Cool Features
- More than 15k games                    
- Pagination and easy navigation 
- Google Authentication 
- Dynamic navigation bar which shows avatar when user is signed in
- Cool footer with project details

### How to run
Clone the project and open it with VSCode. Ensure that you have .env files
in root and in client.                                                           
The root .env should contain values for PORT, DB_URL, AZURE_SAS, STORAGE_NAME and CONTAINER_NAME.
The client .env should contain a REACT_APP_GOOGLE_CLIENT_ID to connect with Google's Authentication API.                                                  
Open a terminal and run the following commands in the root folder:
1. npm run build 
2. npm run start-backend

In another terminal, run this command in root folder:
3.  npm run start-frontend                                       
Navigate to localhost:3000 in a web browser.

### Deploy URL 
https://mygaminglist.azurewebsites.net/

### Database Importer Repo
https://gitlab.com/dawson2223/620-w23/s01/TeamD/mygaminglist_dbtool

### Accessibility Checklist

- Colorblind Friendliness:

The website was tested for colorblind friendliness using the Colorblindly Chrome Extension.
This extension covers various types of colorblindness including:
  -  Trichromacy / Normal
  -  Blue Cone Monochromacy / Achromatomaly
  -  Monochromacy / Achromatopsia
  -  Green-Weak / Deuteranomaly
  -  Green-Blind / Deuteranopia
  -  Red-Weak / Protanomaly
  -  Red-Blind / Protanopia
  -  Blue-Weak / Tritanomaly
  -  Blue-Blind / Tritanopia
Each section of the website was viewed through the lens of each type to ensure adequate visibility
and contrast.

- Tab Navigation

Tab orders were explored to ensure that it follows a logical order.
This ensures that those with limited navigation options are able to
browse the website and can use every feature that the page has to offer
in a way that is not tedious.

- Screen Readers


