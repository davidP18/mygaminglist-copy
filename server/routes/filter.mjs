import express from "express";
import GameModel from "../models/gameModel.js";

const router = express.Router();

// filtering routes for games
router.use(function (req, res, next) {
  try {
    // determines if url is decodable (prevents decoding errors for keyword route)
    decodeURIComponent(req.url);
    next();
  } catch {
    res.status(500).send("URL cannot be decoded!");
  }
});

/**
 * @swagger
 * /keyword/{keyword}:
 *  get:
 *    summary: Get Games by keyword
 *    description: The route treats regex characters as string literals and is a case insensitive search for games.
 *    parameters:
 *      - in: path
 *        name: keyword
 *        required: true
 *        description: Keyword by which a search is done
 *        schema:
 *          type: string
 *          example: Fall
 *    responses:
 *      200:
 *        description: Successfull response returns an array of 7 games matching the search
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Game'
 *              maxItems: 7
 */
router.get("/keyword/:keyword", async (req, res) => {
  // decode keyword from URI before using it in a regex query
  let parsedKeyword = decodeURI(req.params.keyword);

  // find regex characters and if they are present, insert double blackshashes before them
  // ($& option allows the matched regex characters to be kept in the string -> without it, they would be removed)
  parsedKeyword = parsedKeyword.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

  const games = await GameModel.find({
    name: { $regex: parsedKeyword, $options: "i" },
  }).limit(7); // first 7 games will be returned (performance reasons)
  res.json(games);
});

/**
 * Helper method to format categories (used by category routes)
 * @author David Pizzolongo
 * @param {*} categoryStr String containing all category names
 * @returns Array of categories
 */
function getFormattedCategories(categoryStr) {
  // split categories on '/'
  const categoryArr = categoryStr.split("/");
  // convert category array to be init-cap
  const newCategoryArr = categoryArr.map(
    (category) => category[0].toUpperCase() + category.slice(1).toLowerCase()
  );
  return newCategoryArr;
}

/**
 * @swagger
 * /category/count/{categories}:
 *  get:
 *    summary: Get count of games that match the categories provided in the url
 *    description: The route treats regex characters as string literals and is a case insensitive search for games.
 *    parameters:
 *      - in: path
 *        name: categories
 *        required: true
 *        description: One or more categories to count by
 *        schema:
 *          type: array
 *          items:
 *            type: string
 *        example: [ Action, Horror, Online ]
 *    responses:
 *      200:
 *        description: Successfull response returns the number of games matching the categories
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                count:
 *                  type: number
 *                  description: Number of games
 *                  example: 2842
 *      500:
 *        description: Caught an error trying to search inputted categories
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              description: Invalid route message
 *              example: Invalid categories!
 */
router.get("/category/count/*", async (req, res) => {
  try {
    const categoryArr = getFormattedCategories(req.params[0]);
    // $all property requires that all categories in finalArr are matched by the game
    const categoryGamesCount = await GameModel.count({
      tags: { $all: categoryArr },
    });
    res.json({ count: categoryGamesCount });
  } catch {
    res.status(500).send("Invalid categories!");
  }
});

/**
 * @swagger
 * /category/index/{index}/{categories}:
 *  get:
 *    summary: Get categorized games starting from given index
 *    description: The route treats regex characters as string literals and is a case insensitive search for games.
 *    parameters:
 *      - in: path
 *        name: index
 *        required: true
 *        description: Index we want to start by
 *        schema:
 *          type: number
 *        example: 2
 *      - in: path
 *        name: categories
 *        required: true
 *        description: One or more categories to count by
 *        schema:
 *          type: array
 *          items:
 *            type: string
 *        example: [ Action, Horror, Online ]
 *    responses:
 *      200:
 *        description: Successfull response returns the number of games matching the categories
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                count:
 *                  type: number
 *                  description: Number of games
 *                  example: 2842
 *      500:
 *        description: Caught an error trying to search inputted categories
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              description: Invalid route message
 *              example: No categories provided!
 */
router.get("/category/index/:index/*", async (req, res) => {
  try {
    // return maximum of 120 games (for pagination)
    const GAME_LIMIT = 120;
    // parse given index
    const upperRange = parseInt(req.params.index);

    // parsing failed or index is negative --> don't query the db
    if (upperRange != req.params.index || upperRange < 0) {
      res.status(404).send("Invalid game index!");
    } else {
      const categoryArr = getFormattedCategories(req.params[0]);
      const gamesInRange = await GameModel.find({ tags: { $all: categoryArr } })
        .skip(upperRange)
        .limit(GAME_LIMIT);
      res.json(gamesInRange);
    }
  } catch (err) {
    res.status(500).send("No categories provided!");
  }
});

export { router };
