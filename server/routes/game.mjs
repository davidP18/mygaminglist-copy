import express from "express";
import GameModel from "../models/gameModel.js";
import UserModel from "../models/userModel.js";
import fileUpload from "express-fileupload";
import * as filter from "./filter.mjs";

const router = express.Router();
router.use(fileUpload({ createParentPath: true }));
router.use("/filter", filter.router);

/**
 * @swagger
 * /games/all:
 *  get:
 *    summary: Retrieve all Games
 *    description: A query is sent to retrieve every single game in the database
 *    responses:
 *      200:
 *        description: Successfull response returns an array of games
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Game'
 */
router.get("/all", async (req, res) => {
  const games = await GameModel.find().select({ __v: 0 });
  res.json(games);
});

/**
 * @swagger
 * /games/count:
 *  get:
 *    summary: Retrieve the number of Games for pagination
 *    description: A query is done to count the number of games in the DB
 *    responses:
 *      200:
 *        description: Successfull response returns an object of the amount of games
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                count:
 *                  type: number
 *                  example: 65000
 */
router.get("/count", async (req, res) => {
  // returns 0 if db is empty
  const count = await GameModel.count();
  res.json({ count: count });
});

/**
 * @swagger
 * /games/{index}:
 *  get:
 *    summary: Retrieve n number of Games
 *    description: Based on the given index, n number of games is sent back
 *    parameters:
 *      - in: path
 *        name: index
 *        required: true
 *        description: Upper index provided to decide range of games
 *        schema:
 *          type: number
 *          example: 4
 *      - in: query
 *        name: sort
 *        schema:
 *            type: string
 *            example: nameDesc
 *        description: Sort field followed by the sort order (Asc or Desc).
 *    responses:
 *      200:
 *        description: Successful response returns an array of games (sorted only if requested in the query)
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Game'
 *              maxItems: 120
 *      404:
 *        description: Client Side Error due to invalid input 
 *        content:
 *          text/plain:
 *            schema:
 *              type: string
 *              example: Invalid game index, must be a positive integer!
 */
router.get("/:index", async (req, res) => {
  const GAME_LIMIT = 120;
  // parse given index
  const upperRange = parseInt(req.params.index);
  // parsing failed
  if (isNaN(upperRange) || upperRange < 0) {
    res.status(404).send("Invalid game index, must be a positive integer!");
  } else {
    let gamesInRange = GameModel.find()
      .skip(upperRange)
      .limit(GAME_LIMIT);

    const sortType = req.query.sort;
    // sort games if query parameter was found and contains a valid field from the db followed by a sorting order 
    const isValidSortType = sortType?.match(/(name|likes)(Asc|Desc)/g) != null;
    if (isValidSortType) {
      const sortQuery = getSortQuery(sortType);
      gamesInRange = gamesInRange.sort(sortQuery);
    }

    // wait till complete query is executed 
    const finalGames = await gamesInRange.exec();
    res.json(finalGames);
  }
});

// This function returns the sort by clause required by Mongoose to sort documents in the 
// database. It takes as input a valid sortType and returns an object where the key is the sorting
// field and the value is the order to sort by.
function getSortQuery(sortType) {
  // 1=ascending, -1=descending (based on how Mongoose queries work)
  const sortOrder = sortType.includes("Asc") ? 1 : -1;
  // remove sort order from sort type so that the sorting could take place
  const sortField = sortType.replace("Asc", "").replace("Desc", "");

  return { [sortField]: sortOrder };
}

/**
 * @swagger
 * /games/{game}/bookmark:
 *  put:
 *    summary: Modify the 'bookmark' status of the selected game
 *    description: An update is made on the 'bookmark' status, by calling helper functions, for the given user and game
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game being bookmarked 
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's changing the game status
 *                example: newuser@login.com
 *              category:
 *                type: string
 *                description: Name of the category that the game is affected by
 *                example: bookmark
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.put("/:game/bookmark", async (req, res) => {
  updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

/**
 * Asynchronous function which updates the User object in some way depending on the parameters
 * @author Shiv Patel
 * @param {*} category the category which will be getting an update 
 * @param {*} email email of the user affected
 * @param {*} game id of the game which is affected
 */
async function updateObj(category, email, game) {
  let user = await UserModel.findOne({ email: email })
  let copy // Copy of the array we want to updated
  let update // Value of the update happening
  let other  // Other array that might be affected
  let i // index value
  // This switch case addresses all possible categories a user could click
  switch (category) {
    case "bookmark":
      copy = [...user.bookmarks]
      copy = updateGameStatus(copy, game)
      update = { bookmarks: copy }
      break
    // For the playing case, if we add to it, we want the completed to not have the same game
    case "playing":
      copy = [...user.playing]
      other = [...user.completed]
      i = getCopyIndex(other, game)
      if (i >= 0) {
        other.splice(i, 1)
        update = { completed: other }
        await UserModel.findOneAndUpdate({ email: email }, update)
      }
      copy = updateGameStatus(copy, game)
      update = { playing: copy }
      break
    // For the completed case, if we add to it, we want the playing to not have the same game
    case "completed":
      copy = [...user.completed]
      other = [...user.playing]
      i = getCopyIndex(other, game)
      if (i >= 0) {
        other.splice(i, 1)
        update = { playing: other }
        await UserModel.findOneAndUpdate({ email: email }, update)
      }
      copy = updateGameStatus(copy, game)
      update = { completed: copy }
      break
    case "favorite":
      update = { favorite: game }
      if (checkFavorite(user, game)) {
        update = { favorite: null };
      }
      break
    case "like":
      copy = await updateLikes(user.likes, game)
      update = { likes: copy }
      break
    default:
      console.log("In default")
      break
  }
  await UserModel.findOneAndUpdate({ email: email }, update)
}

/**
 * Async function which update the likes of a given game by either increasing or reducing it by 1
 * @author Shiv Patel
 * @param {*} likes Array of games a user has already liked
 * @param {*} gameId id of the game the user has currently pressed the like button for
 * @returns array which is a copy of the likes param, but modified in some way
 */
async function updateLikes(likes, gameId) {
  let copy = [...likes]
  const chosenGame = await GameModel.findOne({ _id: gameId })
  let gameUpdate
  let i = getCopyIndex(copy, gameId)
  // If game is in the array already, we remove it
  if (i >= 0) {
    copy.splice(i, 1)
    gameUpdate = { likes: --chosenGame.likes }
  } else { // If it's not, we add it in
    copy.push(gameId)
    gameUpdate = { likes: ++chosenGame.likes }
  }
  await GameModel.findOneAndUpdate({ _id: gameId }, gameUpdate)
  return copy
}


/**
 * Function which checks if a given game is already inside an array of games
 * @author Shiv Patel
 * @param {*} copy Array of game id's
 * @param {*} game Id of a single game
 * @returns Number representing the index if the game exists, otherwise the return is -1
 */
function getCopyIndex(copy, game) {
  for (let i = 0; i < copy.length; i++) {
    if (copy[i].toString() === game) {
      return i;
    }
  }
  return -1;
}

/**
 * Function which either adds or remove a game from an array
 * @author Shiv Patel
 * @param {*} copy Array containing game id's
 * @param {*} game A single game id 
 * @returns the updated copy parameter
 */
function updateGameStatus(copy, game) {
  let i = getCopyIndex(copy, game)
  if (i >= 0) {
    copy.splice(i, 1)
  } else {
    copy.push(game)
  }
  return copy
}

// This route will update the games in Playing of a user
/**
 * @swagger
 * /games/{game}/playing:
 *  put:
 *    summary: Modify the 'playing' status of the selected game
 *    description: An update is made on the 'playing' status, by calling helper functions, for the given user and game
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game being played 
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's changing the game status
 *                example: newuser@login.com
 *              category:
 *                type: string
 *                description: Name of the category that the game is affected by
 *                example: playing
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.put("/:game/playing", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// This route will update the games in Completed of a user
/**
 * @swagger
 * /games/{game}/completed:
 *  put:
 *    summary: Modify the 'completed' status of the selected game
 *    description: An update is made on the 'completed' status, by calling helper functions, for the given user and game
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game that will be completed 
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's changing the game status
 *                example: newuser@login.com
 *              category:
 *                type: string
 *                description: Name of the category that the game is affected by
 *                example: completed
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.put("/:game/completed", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// This route will update the favorite game of a user
/**
 * @swagger
 * /games/{game}/favorite:
 *  put:
 *    summary: Modify the 'favorite' status of the selected game
 *    description: An update is made on the 'favorite' status by calling helper functions, for the given user and game
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game being favorited 
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's changing the game status
 *                example: newuser@login.com
 *              category:
 *                type: string
 *                description: Name of the category that the game is affected by
 *                example: favorite
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.put("/:game/favorite", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});


/**
 * This function checks if the game that was selected to be favorited, has already been selected or not
 * @param {*} user user object being modified
 * @param {*} game game being affected
 * @returns boolean value indicating if the current favorite is the one being modified or not
 */
function checkFavorite(user, game) {
  try {
    if (user.favorite.toString() === game) {
      return true;
    }
  } catch {
    return false;
  }
}

// This route will update the like status of a game for a user
/**
 * @swagger
 * /games/{game}/like:
 *  put:
 *    summary: Modify the 'like' status of the selected game
 *    description: An update is made on the 'like' status by calling helper functions, for the given user and game
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game being liked 
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's changing the game status
 *                example: newuser@login.com
 *              category:
 *                type: string
 *                description: Name of the category that the game is affected by
 *                example: like
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.put("/:game/like", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

/**
 * @swagger
 * /games/{game}/image:
 *  get:
 *    summary: Retrieve the image of a given game
 *    description: A query is done to retrieve the game based on id, and then a response of the image is sent back
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game we want the image of
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    responses:
 *      200:
 *        description: Sends the image object of a game
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                image: 
 *                  type: string
 *                  example: https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1598556351
 */
router.get("/:game/image", async (req, res) => {
  try {
    let game = await GameModel.findOne({ _id: req.params.game });
    res.json({ image: game.img_url });
  } catch (err) {
    // catches casting errors to ObjectId
    res.status(404).send('Invalid game id!');
  }
});

/**
 * @swagger
 * /games/id/{game}:
 *  get:
 *    summary: Retrieve a specific game object based on id
 *    description: An query is made to retrieve a game based on id
 *    parameters:
 *      - in: path
 *        name: game
 *        required: true
 *        description: ID of the game we want
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    responses:
 *      200:
 *        description: Sends a Game Object
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                game: 
 *                  $ref: '#/components/schemas/Game'
 */
router.get("/id/:game", async (req, res) => {
  try {
    let game = await GameModel.findOne({ _id: req.params.game });
    res.json({ game: game });
  } catch {
    res.status(404).send("Invalid game id!");
  }
});

// This route will update the Game object to contain a comment made by a user
/**
 * @swagger
 * /games/{id}/commment:
 *  post:
 *    summary: Add a comment to the selected game
 *    description: A comment by a given user, will be added to the game object of the given game
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the game we want to add a comment on
 *        schema:
 *          type: string
 *          example: 640e625c9bec121e19035d46
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user who's posting a comment
 *                example: newuser@login.com
 *              comment:
 *                type: string
 *                description: Message left by user when he posted a comment
 *                example: I love this game
 *    responses:
 *      202:
 *        description: The route was called successfully
 */
router.post("/:id/comment", async (req, res) => {

  //initializing the db models we modify alongside a variable to contain their fields
  let game = await GameModel.findOne({ _id: req.params.id })
  let person = await UserModel.findOne({ email: req.body.email })
  let copyComments = []
  let copyGames = []

  // If statement to handle a situation where a game has no comments and its null
  if (game.comments) {
    copyComments = [...game.comments]
  }

  // Update the GameModel with new comment
  copyComments.push({
    user: person._id,
    message: req.body.comment,
  })
  let update = { comments: copyComments }
  await GameModel.findOneAndUpdate({ _id: game }, update)

  // Checks if the user's comments field is null or not
  if (person.comments) {
    copyGames = [...person.comments];
    if (getCopyIndex(copyGames, req.params.id) === -1) {
      copyGames.push(req.params.id);
    }
  } else {
    copyGames.push(req.params.id);
  }

  // Update user model
  update = { comments: copyGames }
  await UserModel.findOneAndUpdate({ email: req.body.email }, update)
  res.sendStatus(202)
})

export { router, getCopyIndex, getSortQuery }
