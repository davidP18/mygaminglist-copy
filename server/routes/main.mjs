import express from "express"
import path from "path"
/**
 * When refreshing the webpage, the server will serve the associated html based on the react router.
 */
const __dirname = path.resolve()
const router = express.Router()

router.use(express.json())

// The following router makes it so that some of the common routes a user will access can be searched by url
router.get(
    ["/game/*", "/users", "/users/profile/*", "/profile", "/profile/*", "/category/*"],
    (req, res) => {
        res.sendFile(path.join(__dirname, "./client/build/index.html"))
    }
)

// 404 route for when user enters a page that doesn't exist
router.use((req, res) => {
    res.status(404).send("Something went wrong");
})

export { router }