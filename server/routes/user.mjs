import express from "express"
import UserModel from "../models/userModel.js"
import GameModel from '../models/gameModel.js'
import fileUpload from "express-fileupload"
import { BlobServiceClient } from "@azure/storage-blob"
import * as dotenv from "dotenv"
dotenv.config()

// Setting up the Blob Storage Client
const sasToken = process.env.AZURE_SAS
const containerName = process.env.CONTAINER_NAME
const storageAccountName = process.env.STORAGE_NAME
const blobService = new BlobServiceClient(
  `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`
)
const containerClient = blobService.getContainerClient(containerName)

// Make it so the route accepts files as uploads
let router = express.Router()
router.use(fileUpload({ createParentPath: true }))

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties: 
 *         username:
 *           type: string
 *           description: Name of the user 
 *           example: No Name
 *         bio:
 *           type: string
 *           description: Information the User has about himself 
 *           example: Hello Everyone I am 20 years old
 *         pfp:
 *           type: string
 *           description: Link to the image of the user's profile picture
 *           example: https://azuretest1935098.blob.core.windows.net/helloblob/image0.jpg
 *         email:
 *           type: string
 *           description: Unique email of the user 
 *           example: noname@domain.com
 *         bookmarks:
 *           type: array
 *           description: An array containing the id's of the games the user bookmarked
 *           example: [640e625c9bec121e19035d46, 640e625c9bec121e19035d47, 640e625c9bec121e19035d48, 640e625c9bec121e19035d49]
 *           items:
 *             type: string
 *             description: A singular game id
 *             example: 640e625c9bec121e19035d47
 *         playing:
 *           type: array
 *           description: An array containing the id's of the games the user is playing
 *           example: [640e625c9bec121e19035d46, 640e625c9bec121e19035d47, 640e625c9bec121e19035d48, 640e625c9bec121e19035d49]
 *           items:
 *             type: string
 *             description: A singular game id
 *             example: 640e625c9bec121e19035d47
 *         completed:
 *           type: array
 *           description: An array containing the id's of the games the user has completed
 *           example: [640e625c9bec121e19035d46, 640e625c9bec121e19035d47, 640e625c9bec121e19035d48, 640e625c9bec121e19035d49]
 *           items:
 *             type: string
 *             description: A singular game id
 *             example: 640e625c9bec121e19035d47
 *         likes:
 *           type: array
 *           description: An array containing the id's of the games the user has liked
 *           example: [640e625c9bec121e19035d46, 640e625c9bec121e19035d47, 640e625c9bec121e19035d48, 640e625c9bec121e19035d49]
 *           items:
 *             type: string
 *             description: A singular game id
 *             example: 640e625c9bec121e19035d47
 *         comments:
 *           type: array
 *           description: An array containing the id's of the games the user has commented on
 *           example: [640e625c9bec121e19035d46, 640e625c9bec121e19035d47, 640e625c9bec121e19035d48, 640e625c9bec121e19035d49]
 *           items:
 *             type: string
 *             description: A singular game id
 *             example: 640e625c9bec121e19035d47
 *         access:
 *           description: Number which defines if a user is an admin or not
 *           example: 1
 *           type: number
 *         favorite:
 *           description: The id of the user's favorite game
 *           type: string
 *           example: 640e625c9bec121e19035d47
 *     Comment:
 *       type: object
 *       properties:
 *         user:
 *           type: string
 *           description: The id of the user who left a comment
 *           example: 640e62b46f0624116b07a805
 *         message:
 *           type: string
 *           description: The message that the user left on the site
 *           example: THIS GAME IS AMAZING
 *     Game:
 *       type: object
 *       properties:
 *         img_url:
 *           type: string
 *           description: URL of the game's image
 *           example: https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1598556351
 *         date:
 *           type: string
 *           description: Date that the game was released
 *           example: Nov 16 2018
 *         developer:
 *           type: string
 *           description: Name of the company which developed the game
 *         publisher:
 *           type: string
 *           description: Name of the company which published the game
 *         desc:
 *           type: string
 *           description: Description of what the game is about
 *         tags:
 *           type: array
 *           description: An array containing all the tags of the game
 *           example: ["Action", "Puzzle", "Multiplayer", "Sci-Fi"]
 *           items:
 *             type: string
 *             description: Name of a tag
 *             example: Action
 *         name:
 *           type: string
 *           description: The name of the game
 *         steam_url:
 *           type: string
 *           description: URL of the associated steam page for the game
 *         like:
 *           type: number
 *           description: The amount of likes a game currently has
 *           example: 5
 *         comments:
 *           type: array
 *           description: An array of the Comments object. This contains all the comments posted for this game
 *           items:
 *             $ref: '#/components/schemas/Comment'
 */


/**
 * @swagger
 * /users/all:
 *  get:
 *    summary: Retrieves all Users
 *    description: A query is made to a database to retrieve all users   
 *    responses:
 *      200:
 *        description: Successfull response returns an array of JSON Users
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/User'
 *      500:
 *        description: The error occured is sent back as a json message
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                  example: Error Message will be sent through this
 */
router.get("/all", async (req, res) => {
  try {
    const users = await UserModel.find().select({ __v: 0 })
    res.status(200).json(users)
  } catch (error) {
    console.log("Failed to get all users")
    res.status(500).json({ message: error.message })
  }
})

/**
 * @swagger
 * /users/{email}:
 *   get:
 *     summary: Retrieves a User with a given email
 *     description: Mongoose Query will find the user with the matching email
 *     parameters:
 *       - in: path
 *         name: email
 *         required: true
 *         description: Email of a user you want to search for
 *         schema:
 *           type: string
 *           example: random@domain.com
 *     responses:
 *       200:
 *         description: A user object is sent
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.get("/:email", async (req, res) => {
  const user = await UserModel.findOne({ email: req.params.email })
  res.status(200).json(user)
})

/**
 * @swagger
 * /users/id/{id}:
 *   get:
 *     summary: Retrieves a User with a given id
 *     description: Mongoose Query will find the user with the matching id
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of a user you want to search for
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Successfull response returns a user object
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.get("/id/:id", async (req, res) => {
  try {
    const user = await UserModel.findOne({ _id: req.params.id });
    res.status(200).json(user);
  }
  catch {
    // mongo throws an error if the ObjectId is invalid
    res.status(404).send('Invalid user id!');
  }
});


/**
 * @swagger
 * /users/name/{username}:
 *  get:
 *    summary: Retrieves Users based on a given username
 *    description: A query is made to a database to retrieve the specified users
 *    parameters:
 *      - in: path
 *        name: username
 *        required: true
 *        description: Username of a user you want to search for
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Successfull response returns an array of user objects
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/User'
 *              maxItems: 7
 */
router.get('/name/:username', async (req, res) => {
  // decode username from URI before using it in a regex query 
  let parsedUsername = decodeURI(req.params.username)

  // escape all regex characters (see filter.mjs for more details)
  parsedUsername = parsedUsername.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")

  const user = await UserModel.find({ username: { $regex: parsedUsername, $options: 'i' } })
    .limit(7)
  res.json(user)
})

/**
 * @swagger
 * /users/login:
 *  post:
 *    summary: Logs a user onto the website
 *    description: A user will be logged into the website, and in the event he doesn't already have an account, one will be created for him
 *    requestBody:
 *      description: Information sent when the request is made
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              username:
 *                type: string
 *                description: Username received from google auth
 *                example: John Wick
 *              profile:
 *                type: string
 *                description: Link to the user's profile picture
 *                example: https://lh3.googleusercontent.com/a/AGNmyxZxjsV7WDRvASbYNP2KAKkGCguFqeI164gF_qCH=s96-c
 *              email:
 *                type: string
 *                description: Username received from google auth
 *                example: newuser@login.com
 *    responses:
 *       202:
 *         description: Successful response will send back the pfp of the user
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 profile:
 *                   type: string
 *                   example: https://azuretest1935098.blob.core.windows.net/helloblob/image0.jpg
 */
router.post("/login", async (req, res) => {
  const user = await UserModel.find({ email: req.body.email })
  if (user.length == 0) {
    const userModel = new UserModel({
      username: req.body.username,
      bio: "No Bio",
      pfp: req.body.profile,
      email: req.body.email,
      bookmarks: [],
      playing: [],
      completed: [],
      likes: [],
      access: 1,
      favorite: null,
    })
    try {
      userModel.save()
    } catch (err) {
      console.log(err)
    }
    res.status(202).json({ profile: req.body.profile })
  } else {
    res.status(202).json({ profile: user[0].pfp })
  }
})

/**
 * @swagger
 * /users/edit:
 *  post:
 *    summary: Edit the information of a corresponding User
 *    description: A Users's object will be updated with the new inputs
 *    requestBody:
 *      description: Information sent when the request is made. 
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                description: Email of the user being edited
 *              bio:
 *                type: string
 *                description: Bio of the user being edited
 *              name:
 *                type: string
 *                description: Name of the user being edited
 *              profile:
 *                type: string
 *                description: Either a link to the user's profile picture, or a file uploaded by user
 *                format: binary
 *    responses:
 *       200:
 *         description: Successful response will send the updated user back
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 */
router.post("/edit", async (req, res) => {
  let profile

  // Try catch for blob storage but also to set the correct profile in case user changed it
  try {
    const blobName = req.files.file.name
    const file = req.files.file
    const blobPublicUrl = `https://${storageAccountName}.blob.core.windows.net/${containerName}/`
    const blobClient = containerClient.getBlockBlobClient(blobName)
    const options = { blobHTTPHeaders: { blobContentType: file.mimetype } }
    await blobClient.uploadData(file.data, options)
    profile = blobPublicUrl + encodeURIComponent(blobName)
  } catch (e) {
    profile = req.body.profile
    console.log("There was no new profile selected")
  }

  // We update the user object based on form data and then send the updated object back
  const update = {
    username: req.body.name,
    email: req.body.email,
    bio: req.body.bio,
    pfp: profile,
  }
  const doc = await UserModel.findOneAndUpdate(
    { email: req.body.email },
    update,
    { new: true }
  )
  res.status(200).json(doc)
})

/**
 * @swagger
 * /users/delete/{email}:
 *  delete:
 *    summary: Delete a User
 *    description: Based on a given email, a User will be deleted from the site 
 *    parameters:
 *      - in: path
 *        name: email
 *        required: true
 *        description: Email of a user you want to delete
 *        schema:
 *          type: string
 *          example: random@domain.com
 *    responses:
 *       200:
 *         description: Successful response deletes the user
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *               example: User successfully deleted
 */
router.delete("/delete/:email", async (req, res) => {
  const user = await UserModel.findOne({ email: req.params.email });
  deleteUserCommments(user);
  deleteUserLikes(user);
  await UserModel.findOneAndDelete({ email: req.params.email });
  res.send('User successfully deleted.');
})
/**
 * This function will take a user and go through all the games he commented on, and then delete those comments
 * @author Shiv Patel
 * @param {*} user Object representing the user affected
 */
async function deleteUserCommments(user) {
  const comments = [...user.comments]
  const user_id = user._id
  for (const id of comments) {
    const game = await GameModel.findOne({ _id: id })
    let resultArr = game.comments.filter(comment => comment.user.toString() !== user_id.toString())
    let update = { comments: resultArr }
    await GameModel.findOneAndUpdate({ _id: id }, update)
  }
}


/**
 * This function will take a user, go through all the games he liked, and then reduce those likes by 1
 * @author Shiv Patel
 * @param {*} user Object representing the user affected
 */
async function deleteUserLikes(user) {
  const likes = [...user.likes]
  for (const id of likes) {
    const game = await GameModel.findOne({ _id: id })
    let likeCount = --game.likes
    let update = { likes: likeCount }
    await GameModel.findOneAndUpdate({ _id: id }, update)
  }
}

export { router, deleteUserLikes }
