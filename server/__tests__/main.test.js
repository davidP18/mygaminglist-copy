import request from 'supertest';
import express from 'express';
import { router } from '../routes/main.mjs';

const app = new express();
app.use('/', router);

describe('Valid Routes', () => {
    test('game route', async () => {
        const res = await request(app).get('/game/123');
        // displays default React page since we are not querying the db 
        expect(res.type).toBe('text/html');
    });

    test('users route', async () => {
        const res = await request(app).get('/users');
        expect(res.type).toBe('text/html');
    });

    test('user profile route', async () => {
        const res = await request(app).get('/game/456');
        expect(res.type).toBe('text/html');
    });

});

describe('Invalid Routes', () => {
    test('game route with no id', async () => {
        const res = await request(app).get('/game');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Something went wrong');
    });

    test('user route with no profile id', async () => {
        const res = await request(app).get('/users/profile');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Something went wrong');
    });

    test('invalid route', async () => {
        const res = await request(app).get('/admin');
        expect(res.statusCode).toBe(404);
    });
});

