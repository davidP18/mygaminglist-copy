import request from 'supertest';
import express from 'express';
import { router, getCopyIndex, getSortQuery } from '../routes/game.mjs';
import GameModel from '../models/gameModel.js';
import mockingoose from 'mockingoose';
import { ObjectId } from 'mongodb';

const app = new express();
app.use('/', router);

/* The following tests use lambda functions to mock the functionality
   of the mongoose find method (no db connection!). Whatever the mock returns
   will be sent as a JSON response from the server to the client. */

describe('Valid Game Routes', () => {
    test('games all route', async () => {
        const mockData = [
            {
                name: "Rust"
            },
            {
                name: "Warframe"
            },
            {
                name: "Resident Evil 3"
            }
        ];

        const finderMock = query => {
            const whereClause = query.getFilter();
            // 'where' clause of query should be empty (we want all games)
            expect(Object.keys(whereClause).length).toBe(0);

            // version key is the only field that should be exempted from the result
            expect(query.projection().__v).toBeFalsy();

            return mockData;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        const res = await request(app).get('/all');
        expect(res.body.length).toBe(3);
        expect(res.body[0].name).toBe('Rust');
    });

    test('games count route', async () => {
        const countMock = query => {
            // no query is created because find method is not called
            const queryObj = query.getFilter();
            expect(Object.keys(queryObj).length).toBe(0);

            return 1000;
        };

        // makes sure that count function is called
        mockingoose(GameModel).toReturn(countMock, 'count');

        const res = await request(app).get('/count');
        expect(res.statusCode).toBe(200);
        // response should contain a json with a count property
        expect(res.type).toMatch('json');
        expect(res.body.count).toBe(1000);
    });

    test('games index route', async () => {
        const mockGames = [
            {
                name: 'Farming Simulator 19'
            },
            {
                name: 'NBA 2K21'
            }
        ];

        const finderMock = query => {
            // get extra query options provided to the find method
            const queryOptions = query.getOptions();
            // skip first 10 games (given in the url)
            expect(queryOptions.skip).toBe(10);
            // maximum of 120 games to be returned 
            expect(queryOptions.limit).toBe(120);

            return mockGames;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        // valid integer
        const res = await request(app).get('/10');
        expect(res.statusCode).toBe(200);
        expect(res.type).toMatch('json');
    });

    test('games index route with decimal number', async () => {
        const mockGames = [
            {
                name: 'Game 8'
            },
            {
                name: 'Game 9'
            }
        ];

        const finderMock = query => {
            const queryOptions = query.getOptions();
            expect(queryOptions.skip).toBe(7);
            expect(queryOptions.limit).toBe(120);

            return mockGames;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        const res = await request(app).get('/7.5');
        expect(res.statusCode).toBe(200);
    });

    test('games index route with valid query parameter', async () => {
        const mockGames = [
            {
                name: 'Rust',
                likes: 1
            },
            {
                name: 'Marvel Avengers',
                likes: 3
            }
        ];

        const finderMock = query => {
            const queryOptions = query.getOptions();
            expect(queryOptions.skip).toBe(2);
            // sort clause should also be in the query
            expect(queryOptions.sort).toEqual({ likes: 1 });

            return mockGames;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        const res = await request(app).get('/2?sort=likesAsc');
        expect(res.statusCode).toBe(200);
    });

    test('games index route with invalid query parameter', async () => {
        const mockGames = [
            {
                name: 'DayZ'
            },
            {
                name: 'Fallout 76'
            }
        ];

        const finderMock = query => {
            const queryOptions = query.getOptions();
            // there should be no sort clause
            expect(queryOptions.sort).toBe(undefined);

            return mockGames;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        // incomplete sort parameter (missing column to sort by)
        const res = await request(app).get('/0?sort=Desc');
        // array of games will still be returned but they will not be sorted
        expect(res.body.length).toBe(2);
    });

    test('games image route', async () => {
        const findOneMock = query => {
            const queryObj = query.getFilter();
            // check that the id was received
            expect(queryObj._id).toBe('123');

            // return sample image url
            return { img_url: 'https://www.google.ca/' };
        };

        mockingoose(GameModel).toReturn(findOneMock, 'findOne');

        const res = await request(app).get('/123/image');
        expect(res.statusCode).toBe(200);
        expect(res.type).toMatch('json');
        expect(res.body.image).toBe('https://www.google.ca/');
    });

    test('game id route', async () => {
        const findOneMock = query => {
            const queryObj = query.getFilter();
            // check that the id was received via query
            expect(queryObj._id).toBe('101');

            // no game matches that id
            return {};
        };

        mockingoose(GameModel).toReturn(findOneMock, 'findOne');

        const res = await request(app).get('/id/101');
        expect(res.statusCode).toBe(200);
        expect(res.type).toMatch('json');
    });

});

describe('Invalid Game Routes', () => {
    test('misspelled route', async () => {
        const res = await request(app).get('/alll');
        expect(res.statusCode).toBe(404);
    });

    test('games index route with negative integer', async () => {
        const res = await request(app).get('/-1');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Invalid game index, must be a positive integer!');
    });

    test('games index route with invalid number', async () => {
        // parsing hello to an int will fail and a 404 code will be returned
        const res = await request(app).get('/hello');
        expect(res.statusCode).toBe(404);
    });

    test('games image route with invalid id', async () => {
        const res = await request(app).get('/hello/image');
        // casting to Object Id will fail
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Invalid game id!');
    });

    test('games id route with invalid id', async () => {
        const res = await request(app).get('/id/invalidId');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Invalid game id!');
    });

});

describe('Helper Method Tests', () => {
    // ensures that getCopyIndex returns the index of a game if it is
    // stored in the array of game ids
    test('getCopyIndex returns 1', () => {
        const gameId = '6425b0c52d492d723c72687d';
        const id1 = new ObjectId('000000000000000000000000');
        const id2 = new ObjectId(gameId);

        const arr = [id1, id2];
        const index = getCopyIndex(arr, gameId);
        // game ids match in the 2nd index of the array
        expect(index).toBe(1);
    });

    // tests that getCopyIndex returns -1 when the game id is not found in the array
    test('getCopyIndex returns -1', () => {
        const gameId = '6425b0c52d492d723c726881';
        const id1 = new ObjectId('000000000000000000000000');
        const id2 = new ObjectId('111111111111111111111111');

        const arr = [id1, id2];
        const index = getCopyIndex(arr, gameId);
        expect(index).toBe(-1);
    });

    test('getSortQuery returns correct sort by clause - ascending order', () => {
        const sortClause = getSortQuery("nameAsc");
        expect(sortClause).toEqual({ name: 1 });
    });

    test('getSortQuery returns sort by clause - descending order', () => {
        const sortClause = getSortQuery("likesDesc");
        expect(sortClause).toEqual({ likes: -1 });
    });
});
