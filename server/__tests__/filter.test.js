import request from "supertest";
import express from "express";
import { router } from "../routes/filter.mjs";
import GameModel from "../models/gameModel.js";
import mockingoose from "mockingoose";

const app = new express();
app.use("/", router);

/* The following tests use lambda functions to mock the functionality
   of the mongoose find method (no db connection!). Whatever the mock returns
   will be sent as a JSON response from the server to the client. */

describe("Valid Filter Routes", () => {
  test("filter by keyword gives 1 result", async () => {
    const mockResult = [
      {
        name: "Among Us",
      },
    ];

    const finderMock = (query) => {
      // access query object passed to mongoose find method
      const queryKeyword = query.getFilter().name;
      expect(queryKeyword.$regex).toBe("among");
      // make sure that the search is case insensitive
      expect(queryKeyword.$options).toBe("i");

      // verifies that names will be included in query result
      expect(query.schema.tree.name).toBeTruthy();

      return mockResult;
    };

    mockingoose(GameModel).toReturn(finderMock, "find");

    // test keyword route with only letters
    const res = await request(app).get("/keyword/among");
    expect(res.body.length).toBe(1);
    expect(res.body[0].name).toBe("Among Us");
  });

  test("filter by keyword gives many results", async () => {
    const mockResult = [
      {
        name: "Resident Evil 3",
      },
      {
        name: "Borderlands 3",
      },
    ];

    // callback for mongoose find method
    const finderMock = (query) => {
      const queryKeyword = query.getFilter().name.$regex;
      expect(queryKeyword).toBe("3");

      return mockResult;
    };

    mockingoose(GameModel).toReturn(finderMock, "find");

    // test route with a number this time
    const res = await request(app).get("/keyword/3");
    expect(res.body.length).toBe(2);
    expect(res.body[0].name).toBe("Resident Evil 3");
    expect(res.body[1].name).toBe("Borderlands 3");
  });

  // this will often happen when the user wants to see a more precise game
  test("search keyword contains spaces and special chars", async () => {
    const mockResult = [
      {
        name: "Among Us - Mini Crewmate Bundle",
      },
    ];

    const finderMock = (query) => {
      const queryKeyword = query.getFilter().name.$regex;
      // checks that decoding of URL parameters worked and regex matches the keyword given
      expect(queryKeyword).toBe("Among Us -");

      return mockResult;
    };

    mockingoose(GameModel).toReturn(finderMock, "find");

    const res = await request(app).get("/keyword/Among Us -");
    expect(res.body.length).toBe(1);
    expect(res.body[0].name).toBe("Among Us - Mini Crewmate Bundle");
  });

  test("keyword route with regex pattern", async () => {
    const finderMock = (query) => {
      const queryKeyword = query.getFilter().name.$regex;
      // checks that escaping of regex characters works well
      expect(queryKeyword).toBe("\\[A-Za-z\\]\\+");

      return []; // no game contains this string literal
    };

    mockingoose(GameModel).toReturn(finderMock, "find");

    // tests route with many regex characters ([] and +)
    const res = await request(app).get("/keyword/[A-Za-z]+");
    expect(res.body.length).toBe(0);
  });

  test("keyword route with regex character", async () => {
    // game that has regex chars '(' and ')', both should be escaped
    const gameMock = [
      {
        name: "FIFA 23 (Ultimate Edition)",
      },
    ];

    const finderMock = (query) => {
      const queryKeyword = query.getFilter().name.$regex;
      // checks that decoding of URL parameters worked and regex matches the keyword given
      expect(queryKeyword).toBe("FIFA 23 \\(Ultimate Edition\\)");

      return gameMock;
    };

    mockingoose(GameModel).toReturn(finderMock, "find");

    const res = await request(app).get("/keyword/FIFA 23 (Ultimate Edition)");
    expect(res.statusCode).toBe(200);
    expect(res.body[0].name).toBe("FIFA 23 (Ultimate Edition)");
  });

  test("filter by single category", async () => {
    const countMock = (query) => {
      // '$all' property contains an array of the categories from the url
      const queryCategories = query.getFilter().tags.$all;
      expect(queryCategories.length).toBe(1);
      // category is capitalized to match the one stored in db
      expect(queryCategories[0]).toBe("Action");

      return 25000; // around 25k Action games in our db
    };

    mockingoose(GameModel).toReturn(countMock, "count");

    const res = await request(app).get("/category/count/action");
    expect(res.statusCode).toBe(200);
    // json response with count of games
    expect(res.type).toMatch("json");
    expect(res.body.count).toBe(25000);
  });

  test("filter by multiple categories", async () => {
    const mockResult = [
      {
        name: "Crusader Kings III",
      },
    ];

    const countMock = (query) => {
      const queryCategories = query.getFilter().tags.$all;
      expect(queryCategories.length).toBe(2);
      expect(queryCategories[0]).toBe("Strategy");
      expect(queryCategories[1]).toBe("Multiplayer");

      return mockResult.length;
    };

    mockingoose(GameModel).toReturn(countMock, "count");

    // two categories provided, both will be involved in the query
    const res = await request(app).get("/category/count/strategy/multiplayer");
    expect(res.statusCode).toBe(200);
    expect(res.body.count).toBe(1);
  });

  test('category index route', async () => {
    const mockGames = [
      {
        name: 'Game 5'
      },
      {
        name: 'Game 6'
      }
    ];

    const finderMock = query => {
      // get extra query options provided to the find method
      const queryOptions = query.getOptions();
      // skip 0 games (given in the url)
      expect(queryOptions.skip).toBe(0);
      // maximum of 120 games  
      expect(queryOptions.limit).toBe(120);

      // get categories stored in the query 
      const categoryArr = query.getFilter().tags.$all;
      expect(categoryArr.length).toBe(1);
      // category should match db format
      expect(categoryArr[0]).toBe('Free');

      return mockGames;
    };

    mockingoose(GameModel).toReturn(finderMock, 'find');

    // valid index and category
    const res = await request(app).get('/category/index/0/fRee');
    expect(res.statusCode).toBe(200);
    expect(res.type).toMatch('json');
  });
});

describe("Invalid Filter Routes", () => {
  // no keyword provided
  test("incomplete keyword route", async () => {
    const res = await request(app).get("/keyword/");
    expect(res.statusCode).toBe(404);
  });

  test("invalid keyword route (undecodable URI)", async () => {
    const res = await request(app).get("/keyword/%");
    expect(res.statusCode).toBe(500);
    expect(res.text).toBe("URL cannot be decoded!");
  });

  test("incomplete category count route", async () => {
    // no categories specified; error code 500 will be returned
    const res = await request(app).get("/category/count/");
    expect(res.statusCode).toBe(500);
    expect(res.text).toBe("Invalid categories!");
  });

  test("incomplete category index route", async () => {
    // no categories provided
    const res = await request(app).get("/category/index/50/");
    expect(res.statusCode).toBe(500);
    expect(res.text).toBe("No categories provided!");
  });

  test("category index route with negative index", async () => {
    // negative integer provided
    const res = await request(app).get("/category/index/-7/");
    expect(res.statusCode).toBe(404);
    expect(res.text).toBe("Invalid game index!");
  });

  test("category index route with decimal number index", async () => {
    // index must be a whole number
    const res = await request(app).get("/category/index/100.5/");
    expect(res.statusCode).toBe(404);
    expect(res.text).toBe("Invalid game index!");
  });

  test("category index route with invalid index", async () => {
    const res = await request(app).get("/category/index/index123/");
    expect(res.statusCode).toBe(404);
    expect(res.text).toBe("Invalid game index!");
  });

  test("invalid category route", async () => {
    const res = await request(app).get("/category/");
    expect(res.statusCode).toBe(404);
  });
});
