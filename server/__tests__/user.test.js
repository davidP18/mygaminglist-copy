import request from 'supertest';
import express from 'express';
import { router, deleteUserLikes } from '../routes/user.mjs';
import GameModel from '../models/gameModel.js';
import UserModel from '../models/userModel.js';
import mockingoose from 'mockingoose';
import { ObjectId } from 'mongodb';

const app = new express();
app.use('/', router);

/**
 * Test suite for all valid user routes. Database will be mocked so that there is no actual DB connection.
 */
describe('Valid User Routes', () => {
  test('get all users route', async () => {
    const mockData = [
      {
        username: "Alex",
        email: "alex@gmail.com"
      },
      {
        username: "John",
        email: "john@gmail.com"
      },
      {
        username: "Matt",
        email: "matt@gmail.com"
      }
    ];

    const finderMock = query => {
      const whereClause = query.getFilter();
      // 'where' clause of query should be empty (we want all users)
      expect(Object.keys(whereClause).length).toBe(0);

      // version key is the only field that should be exempted from the result
      expect(query.projection().__v).toBeFalsy();

      return mockData;
    }

    mockingoose(UserModel).toReturn(finderMock, 'find');

    const res = await request(app).get('/all');
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(3);
    expect(res.body[0].username).toBe('Alex');
  });

  test('get user by email route', async () => {
    const mockData = [{
      username: "John",
      email: "john@gmail.com"
    }]

    const finderMock = query => {
      // access query object passed to mongoose find method 
      const queryKeyword = query.getFilter().email;
      expect(queryKeyword).toBe('john@gmail.com');
      return mockData;
    }

    mockingoose(UserModel).toReturn(finderMock, 'findOne');

    const res = await request(app).get('/john@gmail.com')
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(1);
    expect(res.body[0].username).toBe('John');
  });

  test('get user by id route', async () => {
    const mockData = [{
      _id: 'abc123',
      username: "John",
      email: "john@gmail.com"
    }];

    const finderMock = query => {
      // access query object passed to mongoose find method 
      const queryKeyword = query.getFilter()._id;
      expect(queryKeyword).toBe('abc123');
      return mockData;
    }

    mockingoose(UserModel).toReturn(finderMock, 'findOne');

    const res = await request(app).get('/id/abc123')
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(1);
    expect(res.body[0].username).toBe('John');
  });

  test('get user by username route with actual username', async () => {
    const finderMock = query => {
      const queryUsername = query.getFilter().username;
      expect(queryUsername.$regex).toBe('David');
      // search should be case insensitive (ignores uppercase and lowercase)
      expect(queryUsername.$options).toBe('i');

      const queryOptions = query.getOptions();
      // limit of 7 users to display under the search bar
      expect(queryOptions.limit).toBe(7);

      // david matches 'David' (case insensitive)
      return [{ username: 'david', email: 'email@hotmail.com' }];
    }

    mockingoose(UserModel).toReturn(finderMock, 'find');

    // actual username provided (no regex pattern)
    const res = await request(app).get('/name/David');
    expect(res.statusCode).toBe(200);
    expect(res.type).toMatch('json');
    expect(res.body[0].username).toBe('david');
  });

  test('get user by username route with regex pattern', async () => {
    const finderMock = query => {
      const queryKeyword = query.getFilter();
      expect(queryKeyword.username.$regex).toBe('\\.\\+');

      const queryOptions = query.getOptions();
      // limit of 7 users to display under the search bar
      expect(queryOptions.limit).toBe(7);

      // no username matches a string literal of '.+'
      return [];
    }

    mockingoose(UserModel).toReturn(finderMock, 'find');

    // regex pattern detected and so the characters will be escaped
    const res = await request(app).get('/name/.+');
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(0);
  });

});

/**
 * Test suite for helper functions used by the routes. Database will be mocked so that there is no actual DB connection.
 */
describe('Helper Function Tests', () => {
  test('deletion of user likes', async () => {
    const user = {
      username: 'User1',
      likes: [new ObjectId('6425b0c62d492d723c72688b')]
    };

    // call helper function to delete user likes when user no longer exists
    deleteUserLikes(user);

    // mock functions follow 

    const findOneMock = query => {
      // get id passed to game query 
      const queryId = query.getFilter()._id;
      expect(queryId.toString()).toBe('6425b0c62d492d723c72688b');

      const game = {
        _id: '6425b0c62d492d723c72688b',
        name: 'Dead by Daylight',
        likes: 1
      };

      return game;
    };

    mockingoose(GameModel).toReturn(findOneMock, 'findOne');

    const updateMock = updateObj => {
      // likes should have decreased to 0 
      expect(updateObj.getUpdate().likes).toBe(0);
    };

    mockingoose(GameModel).toReturn(updateMock, 'findOneAndUpdate');
  });

});

/**
 * Test suite for all invalid user routes. 
 */
describe('Invalid User Routes', () => {
  test('route does not exist', async () => {
    const res = await request(app).get('/');
    expect(res.statusCode).toBe(404);
  });
});