import { Int32 } from "mongodb";
import { Schema, Types, model } from "mongoose";

// Creates Users collection by defining a schema of strict mongoose types.
const userSchema = new Schema({
    username: String,
    bio: String,
    pfp: String,
    email: String,
    bookmarks: [{ type: Types.ObjectId }],
    playing: [{ type: Types.ObjectId }],
    completed: [{ type: Types.ObjectId }],
    likes: [{ type: Types.ObjectId }],
    comments: [{ type: Types.ObjectId }],
    access: Number,
    favorite: Types.ObjectId,
});

// export Users collection 
export default model('User', userSchema);