import { Schema, Types, model } from "mongoose";

// Creates Games collection by defining a schema of strict mongoose types.
const gameSchema = new Schema({
    img_url: String,
    date: String,
    developer: String,
    publisher: String,
    desc: String,
    tags: [{ type: String }],
    name: String,
    steam_url: String,
    likes: { type: Number, default: 0 },
    comments: [{
        user: Types.ObjectId,
        message: String
    }]
});

// create indeces in order to make sorting more efficient for game titles and game rating
gameSchema.index({ name: 1 });
gameSchema.index({ likes: 1 });

// export collection so it can be accessible by the api routes
export default model('Game', gameSchema);