import express from "express"
import * as game from "./routes/game.mjs"
import * as user from "./routes/user.mjs"
import * as main from "./routes/main.mjs"
import compression from 'compression'
import swaggerJSDoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'

const app = express()

// Initializing swagger variables
const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Express API for MyGamingList',
    version: '1.0.0',
    description: 'This is an api which allows us to query information about our Users and Games, \n in order to accomplish various functionalities',
    contact: {
      name: 'Team D',
    },
  },
  servers: [
    {
      url: 'http://localhost:3000',
      description: 'Front-End Server',
    },
    {
      url: 'http://localhost:3001',
      description: 'Back-End Server',
    },
    {
      url: 'https://mygaminglist.azurewebsites.net',
      description: 'Production server',
    },
  ],
};

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ['./server/routes/*.mjs'],
};

const swaggerSpec = swaggerJSDoc(options);

app.use(compression())
// app.use(function (req, res, next) {
//   res.set("Cache-control", "public, max-age=31536000");
//   next();
// });

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(express.static("./client/build"))
app.use("/games", game.router)
app.use("/users", user.router)
app.use("/", main.router)

export default app