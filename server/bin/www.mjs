import app from "../app.mjs";
import mongoose, { mongo } from 'mongoose';
import dotenv from "dotenv";
dotenv.config();
mongoose.set('strictQuery', false);

const port = process.env.PORT || 3001;

// Connect to db and if successful, listen on the port.
connectToDB().catch(() => {
    console.log("Connection to db failed!!!");
    process.exit(1);
});
app.listen(port, () => {
    console.log(`Server listening on port ${port}!`);
});

// Connect to database cluster asynchronously.
async function connectToDB() {
    await mongoose.connect(process.env.DB_URL);
    console.log("Connected to DB!");
}