// Utility methods for cataloguing games 

// This function returns a FormData object (similar to JSON) that contains the user's data and the action they performed
// (like, bookmark, etc.). This will be sent in a PUT request to the server to update the database.
const getFormData = (email, categoryType) => {
    const formData = new FormData();
    formData.append("email", email);
    formData.append("category", categoryType);
    return formData;
};

// Utility function to catalogue games in a user's account. 
const postToDB = async (userData, gameId, catalogueType) => {
    const resp = await fetch(`/games/${gameId}/${catalogueType}`, {
        body: userData,
        method: "put",
    });
    return resp;
};

export { getFormData, postToDB };
