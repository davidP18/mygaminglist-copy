import { Link } from "react-router-dom";
import "../styles/UserCard.css"

/**
 * This component represent an individual user card containing the user pfp, username and bio.
 * @author Christopher Dagenais
 * @param props contains user properties such as id, username, pfp, id, etc
 * @returns a single user card div
 */
function UserCard({ user, deleteUser, banVisible }) {

  return (
    <div className="user-card">
      <Link to={`profile/${user._id}`} state={{ user }}>
        <img className="user-pfp" src={user.pfp} alt="profile avatar"></img>
      </Link>

      <div className="user-name-and-bio">
        <div className="user-name-container">
          {user.username}
          <button className="user-ban-button" onClick={(e) => { deleteUser(e, user.email) }} style={{ visibility: banVisible ? 'visible' : 'hidden' }}>Delete</button>
        </div>
        <div className="user-bio-container">
          <p className="user-bio-text">{user.bio}</p>
        </div>
      </div>
    </div>
  );
}

export default UserCard;