import React from "react";
import "../styles/UserProfile.css";
import BookmarkRow from "./BookmarkRow";
import PlayingRow from "./PlayingRow";
import CompletedRow from "./CompletedRow";

/**
 * The GameRows component will display a row for bookmarked games,
 * currently playing games and completed games. Each row has its separate component
 * where it manages its own state. This minimizes the number of re-renders and allows
 * each component to fetch independently without waiting for the other one to finish.
 * @author Shiv Patel and David Pizzolongo
 * @returns div containing 3 rows of games
 */
function GameRows() {
  return (
    <>
      <BookmarkRow />
      <hr></hr>
      <PlayingRow />
      <hr></hr>
      <CompletedRow />
    </>
  );
}

export default GameRows;
