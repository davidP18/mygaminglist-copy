import React, { useState, useContext, useEffect } from 'react'
import { UserProfileContext } from "./UserProfile";
import { useTranslation } from 'react-i18next';
import { Link } from "react-router-dom";

/**
 * This component fetches and displays the images corresponding to
 * the user's bookmarked games. The first 6 games will be shown, but 
 * the user can click on the See All option, which will link to the  
 * DisplayProfileGames page.
 * @author David Pizzolongo
 */
function BookmarkRow() {
    const { user } = useContext(UserProfileContext);
    const [bookmarks, setBookmarks] = useState([]);
    const { t } = useTranslation();

    useEffect(() => {
        async function fetchBookmarks() {
            const bookmarkImgs = await Promise.all(
                user.bookmarks.slice(0, 6).map(async (gameId) => {
                    let resp = await fetch(`/games/${gameId}/image`);
                    let json = await resp.json();
                    let image = await json.image;
                    return image;
                })
            );
            setBookmarks(bookmarkImgs);
        }

        fetchBookmarks();
    }, [user]);

    return (
        <div className="game-row-div">
            <h3 className='game-row-title'>{t("bookmark")}</h3>
            <Link to="bookmarks" state={{ games: user.bookmarks }} className='see-all-link'>
                <button className="seeAll">See All </button>
            </Link>
            <section className="game-container">
                {bookmarks.map((url, index) => {
                    return (
                        <img
                            src={url}
                            alt="Game header from Steam"
                            className="gameImage"
                            key={index}
                        />
                    );
                })}
            </section>
        </div>
    )
}

export default BookmarkRow