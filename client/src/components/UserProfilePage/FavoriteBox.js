/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from 'react';
import { UserProfileContext } from "./UserProfile";
import { useTranslation } from "react-i18next";
import defaultImage from "../../assets/image-banner.jpg";

/**
 * This component renders a div containing the game header of a user's favorite game.
 * If the user is viewing their own profile, there will be instructions displayed below their favorite
 * game explaining to them how they can change it if ever they want to.
 * @param {showFavText} boolean variable that indicates if we should show the fav game instructions
 * @author Shiv Patel and David Pizzolongo 
 */
function FavoriteBox({ showFavText }) {
    const { user } = useContext(UserProfileContext);
    const { t } = useTranslation();
    const [favorite, setFavorite] = useState(null);
    const [favoriteText, setFavoriteText] = useState("");

    useEffect(() => {
        if (user.favorite != null) {
            fetchFavoriteImg(user.favorite);
            setFavoriteText(t("has_fav_game_text"));
        }
        else {
            setFavorite(defaultImage);
            setFavoriteText(t("does_not_have_fav_game_text"));
        }
    }, [user.favorite]);

    /**
     * This async function fetches and sets the image url of the user's favorite game. 
     * @param {gameId} id of the favorite game
     */
    async function fetchFavoriteImg(gameId) {
        let resp = await fetch(`/games/${gameId}/image`);
        let json = await resp.json();
        let image = await json.image;
        setFavorite(image);
    }

    return (
        <div className="faveBox">
            <h2>{t("favorite_game")}</h2>
            <img src={favorite ? favorite : defaultImage} alt="Favorite game header" className="favorite"></img>
            {/* show instructions on how to change fav game only if the user is viewing their own profile */}
            {showFavText && (
                <p>
                    <small>{favoriteText}</small>
                </p>
            )}
        </div>
    )
}

export default FavoriteBox