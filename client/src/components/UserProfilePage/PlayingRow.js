import React, { useState, useContext, useEffect } from 'react'
import { UserProfileContext } from "./UserProfile";
import { useTranslation } from 'react-i18next';
import { Link } from "react-router-dom";

/**
 * This component fetches and displays the images corresponding to
 * the user's currently playing games. The first 6 games will be shown, but 
 * the user can click on the See All option, which will link to the  
 * DisplayProfileGames page.
 * @author David Pizzolongo
 */
function PlayingRow() {
    const { user } = useContext(UserProfileContext);
    const [playing, setPlaying] = useState([]);
    const { t } = useTranslation();

    useEffect(() => {
        async function fetchPlaying() {
            const playingImgs = await Promise.all(
                user.playing.slice(0, 6).map(async (gameId) => {
                    let resp = await fetch(`/games/${gameId}/image`);
                    let json = await resp.json();
                    return json.image;
                })
            );
            setPlaying(playingImgs);
        }

        fetchPlaying();
    }, [user]);

    return (
        <div className="game-row-div">
            <h3 className='game-row-title'>{t("playing")}</h3>
            <Link to="playing" state={{ games: user.playing }} className='see-all-link'>
                <button className="seeAll">See All </button>
            </Link>
            <section className="game-container">
                {playing.map((url, index) => {
                    return (
                        <img
                            src={url}
                            alt="Game header from Steam"
                            className="gameImage"
                            key={index}
                        />
                    );
                })}
            </section>
        </div>
    );
}

export default PlayingRow