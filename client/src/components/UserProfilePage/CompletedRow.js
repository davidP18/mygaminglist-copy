import React, { useState, useContext, useEffect } from 'react'
import { UserProfileContext } from "./UserProfile";
import { useTranslation } from 'react-i18next';
import { Link } from "react-router-dom";

// Displays the first 6 completed games saved in the user's profile.
function CompletedRow() {
    const { user } = useContext(UserProfileContext);
    const [completed, setCompleted] = useState([]);
    const { t } = useTranslation();

    useEffect(() => {
        async function fetchCompleted() {
            const completedImgs = await Promise.all(
                user.completed.slice(0, 6).map(async (gameId) => {
                    let resp = await fetch(`/games/${gameId}/image`);
                    let json = await resp.json();
                    return json.image;
                })
            );
            setCompleted(completedImgs);
        }

        fetchCompleted();
    }, [user]);

    return (
        <div className="game-row-div">
            <h3 className='game-row-title'>{t("completed")}</h3>
            <Link to="completed" state={{ games: user.completed }} className='see-all-link'>
                <button className="seeAll">See All </button>
            </Link>
            <section className="game-container">
                {completed.map((url, index) => {
                    return (
                        <img
                            src={url}
                            alt="Game header from Steam"
                            className="gameImage"
                            key={index}
                        />
                    );
                })}
            </section>
        </div>
    )
}

export default CompletedRow