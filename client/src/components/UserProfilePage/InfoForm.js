import React, { useContext, useEffect } from "react";
import { useState } from "react";
import "../styles/InfoForm.css";
import { useTranslation } from "react-i18next";
import { UserProfileContext } from "./UserProfile";
/**
 * Form component used to update and set user data on web page
 * @author Shiv Patel
 * @param user contains user information like bio, username, pfp
 * @returns form for modifying profile info when clicking Edit button
 */
function InfoForm() {
  const { user } = useContext(UserProfileContext);
  const [edit, setEdit] = useState(false);
  const { t } = useTranslation();
  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    const { bio, username, email, pfp } = user;
    setUserInfo(prevObj => ({ ...prevObj, bio, name: username, email, profile: pfp }));
  }, [user]);

  /**
   * Function which is called when a form is submitted. Will set various states and post data.
   * @param {*} e event triggered
   */
  async function handleSubmit(e) {
    e.preventDefault();

    // Initializing the data to be sent to the api
    let formData = new FormData();
    formData.append("email", userInfo.email);
    formData.append("bio", userInfo.bio);
    formData.append("name", userInfo.name);
    // determine if profile pic was changed (if so, json key will be 'file' instead of 'profile')
    const profileKey = userInfo.profile === user.pfp ? "profile" : "file";
    formData.append(profileKey, userInfo.profile);

    setEdit(!edit);
    try {
      const resp = await fetch("/users/edit", {
        body: formData,
        method: "post",
      });
      let json = await resp.json();
      setUserInfo(prevObj => ({ ...prevObj, profile: json.pfp }));
    } catch {
      console.log("Failed to edit the user profile");
    }
  }

  /**
   * Helper function that sets the value of the name state
   * @param {*} e event triggered
   */
  function nameHandler(e) {
    setUserInfo(prevObj => ({ ...prevObj, name: e.target.value }));
  }

  /**
   * Helper function that sets the value of the bio state
   * @param {*} e event triggered
   */
  function bioHandler(e) {
    setUserInfo(prevObj => ({ ...prevObj, bio: e.target.value }));
  }

  /**
   * Helper function that sets the value of the profile state
   * @param {*} e event triggered
   */
  function fileSelectedHandler(e) {
    setUserInfo(prevObj => ({ ...prevObj, profile: e.target.files[0] }));
  }

  // If user is trying to edit the page, we display a form, otherwise, we display a static component
  if (edit) {
    return (
      <form onSubmit={handleSubmit} className="form">
        <fieldset>
          <label>
            {" "}
            {t("change_name")}
            <input
              type="text"
              name="username"
              value={userInfo.name}
              onChange={nameHandler}
              minlength="3"
              maxlength="30"
              required
            ></input>
          </label>

          <label>
            {" "}
            {t("change_bio")}
            <input
              type="text"
              name="bio"
              value={userInfo.bio}
              onChange={bioHandler}
              minlength="1"
              maxlength="65"
            ></input>
          </label>

          <label>
            {" "}
            {t("email")}
            <input type="email" name="email" value={userInfo.email} readonly></input>
          </label>

          <label htmlFor="profile">
            {" "}
            {t("change_picture")}
            <input
              type="file"
              name="profile"
              id="profile"
              accept="image/*"
              onChange={fileSelectedHandler}
            />
          </label>

          <button type="submit">{t("profile_submit")}</button>
        </fieldset>
      </form >
    );
  }
  return (
    <>
      <h2>{t("profile_page")}</h2>
      <div>
        <img src={userInfo.profile} alt="User Profile" className="pfp" />
      </div>
      <div>
        <p>
          {userInfo.name}
          <br />
          {userInfo.bio}
          <br />
        </p>
        <br />
        {(JSON.parse(localStorage.getItem("loginData"))?.email === userInfo.email) && (
          <button
            onClick={() => {
              setEdit(!edit);
            }}
            id="edit_button"
          >
            {" "}
            {t("edit")}{" "}
          </button>
        )}
      </div>
    </>
  );
}

export default InfoForm;
