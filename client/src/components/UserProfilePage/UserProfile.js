/* eslint-disable react-hooks/exhaustive-deps */
import React, { createContext } from "react";
import { useState, useEffect } from "react";
import GameRows from "./GameRows";
import "../styles/UserProfile.css";
import InfoForm from "./InfoForm";
import { useLocation } from "react-router-dom";
import FavoriteBox from "./FavoriteBox";

export const UserProfileContext = createContext(null);

/**
 * This component displays a specific user's profile page
 * @author Shiv Patel
 * @returns profile page
 */
function UserProfile() {
  // get props using the current Location object
  let { state: userProps } = useLocation();
  const [user, setUser] = useState(userProps ? userProps.user : {});

  // initialize email with the one from props or an empty string
  let email = userProps?.user.email || "";
  // gets email from Local Storage if a user is signed in
  const myEmail = JSON.parse(localStorage.getItem("loginData"))?.email;
  // isMyAccount is true when the email from props is their own or when they enter '/profile' in the address bar
  const isMyAccount = myEmail === email || (myEmail != null && window.location.pathname === "/profile");
  const isValidUser = userProps != null || isMyAccount;

  // This function will fetch the user's data, including their catalogued games.
  async function fetchData() {
    try {
      // if user is viewing their own profile, use their email from Local Storage
      email = myEmail;
      let resp = await fetch(`/users/${email}`);
      let json = await resp.json();
      setUser(json);
    } catch {
      console.log("Failed to fetch user");
    }
  }

  // useEffect will set user state when a user's profile is opened
  useEffect(() => {
    // if the user is not logged in, don't do anything
    if (!isValidUser) return;

    if (isMyAccount) {
      fetchData();
    }
    else {
      setUser(userProps.user);
    }
  }, [userProps]);

  if (isValidUser) {
    return (
      <UserProfileContext.Provider value={{ user }}>
        <div className="flex-container" id="main-container">
          <div className="flex-left">
            <div className="infoBox">{user.username && <InfoForm />}</div>
            <FavoriteBox showFavText={isMyAccount} />
          </div>
          <div className="flex-right">{user.username && <GameRows />}</div>
        </div>
      </UserProfileContext.Provider>
    );
  }
  return (
    <p className="error-msg">
      {window.location.pathname === "/profile" ?
        (<span>
          &#10060; You are not logged in. Sign in {" "}
          <a href="https://accounts.google.com/" rel="noreferrer" target="_blank">HERE</a>
          &#10060;
        </span>) :
        (<span>&#10060; User page does not exist. Return to <a href="/">Home</a> &#10060;</span>)}
    </p>
  );
}

export default UserProfile;
