import React from 'react';
import './styles/Tooltip.css';

/**
 * This component will appear when hovering over each GameCard. 
 * It contains the specific game's title and description (the 'About This Game'
 * message is removed from the start of each game description).
 * @author David Pizzolongo
 * @param {gameTitle} 
 * @param {gameDesc}  
 * @returns Tooltip div container
 */
function Tooltip({ gameTitle, gameDesc }) {
    return (
        <div className='tooltip'>
            <h2>{gameTitle}</h2>
            <p className='tooltipText'>
                {gameDesc.replaceAll(new RegExp('About (the|This) Game', 'g'), '')}
            </p>
        </div >
    )
}

export default Tooltip