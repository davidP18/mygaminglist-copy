/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/SearchBar.css";
import { useTranslation } from "react-i18next";
import axios from 'axios';
/**
 * This functional component contains a form to search for games or users by keyword
 * and username respectively. It renders a datalist to show an auto-complete list of results
 * that match the user's input. The form also contains a search button to navigate to the specific game page.
 * @author David Pizzolongo
 * @param placeholder is the text inside the search bar to indicate what to type.
 * @returns search bar
 */
const SearchBar = React.memo(({ placeholder }) => {
  // stores newly fetched data from db
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  // contains reference to input tag in Virtual DOM 
  const inputRef = useRef();
  const [input, setInput] = useState("");
  // if a variation of the word 'game' is in the placeholder text, match() will return an array of all the matches
  const isGamePage = placeholder.match(/(jeu|juego|game)/g) != null;
  const { t } = useTranslation();

  // clear datalist array when page changes from games to users
  useEffect(() => setData([]), [placeholder]);

  // alternate between showing input text as black/red as user enters a game or user
  useEffect(() => {
    // show input text in black if it is valid (stored in the db) or if user hasn't typed anything yet
    if (input === "" || data.indexOf(input) >= 0) {
      inputRef.current.style.color = "black";
    }
    else if (data.indexOf(input) === -1) {
      inputRef.current.style.color = "red";
    }
  }, [input, data]);

  // This useEffect is called when the user enters something new in the search bar.
  // It will make a GET request to retrieve the games or users that match the search input
  // and update the state of the component so that the datalist element can show the updated results. 
  useEffect(() => {
    // remove trailing whitespace 
    const inputTrimmed = input.trim();
    // only fetch if user entered between 1 and 50 characters
    if (inputTrimmed.length < 1 || inputTrimmed.length > 50) {
      return;
    }
    const controller = new AbortController();
    // api url differs based on what page we are on
    const url = (isGamePage ? "/games/filter/keyword/" : "/users/name/") + inputTrimmed;

    axios.get(url, {
      signal: controller.signal
    }).then(res => {
      const newData = res.data;
      if (isGamePage) {
        // get game titles from json response
        const games = newData.map(game => game.name);
        setData(games);
      }
      else {
        const usernames = newData.map(user => user.username);
        setData(usernames);
      }
    }).catch(e => {
      if (axios.isCancel(e)) return;
      console.log("Network Error. Failed to fetch new data!");
    });

    // return clean-up function (cancel old fetch request if one was still pending)
    return () => controller.abort();
  }, [input]);

  /**
   * The following function is called when the user clicks the Search button.
   * If the game title entered is in the datalist, it is valid and a fetch will occur
   * to get the whole game object. We will then use this as props to navigate to the game's detail page
   * and display all the game's details.
   * @param e represents the element that the function is called upon
   * @returns
   */
  async function handleSubmit(e) {
    e.preventDefault();
    // input is not in the datalist, so don't fetch
    if (data.indexOf(input) === -1) {
      return;
    }

    try {
      if (isGamePage) {
        // fetch entered game
        const response = await fetch("/games/filter/keyword/" + input);
        if (response.ok) {
          const game = (await response.json())[0];
          navigate("game/" + game._id, { state: { game: game } });
        }
      } else {
        // fetch entered user
        const response = await fetch("/users/name/" + input);
        if (response.ok) {
          const user = (await response.json())[0];
          navigate("users/profile/" + user._id, {
            state: { user },
          });
        }
      }
    } catch {
      console.log("Failed to fetch database data!");
    }

    // clear input
    setInput("");
  }

  // returns form, containing search bar, search button and datalist elements
  return (
    <form onSubmit={handleSubmit}>
      <input
        id="searchBar"
        required={true}
        type="search"
        // removes cached searches from browser (they may be invalid games)
        autoComplete="off"
        placeholder={placeholder}
        list="searchData"
        value={input}
        onChange={e => setInput(e.target.value)}
        ref={inputRef}
      />
      <datalist id="searchData">
        {data.map((dataField, index) => (
          <option key={index} value={dataField} />
        ))}
      </datalist>
      <input type="submit" value={t("search_btn_text")} id="searchBtn" />
    </form>
  );
});

export default SearchBar;
