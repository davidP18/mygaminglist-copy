import "../../App.css";
import NavBar from "./NavBar";
import Footer from "./Footer";
import React, { useEffect, createContext } from "react";
import { Outlet } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useState } from "react";

export const AppContext = createContext(null);

/**
 * Root component that contains the 3 elements that constitute our website (navbar, main, footer)
 * @returns the entire web page
 */
export default function RootLayout() {
  // keeps track of sort type that user enters so we only fetch games when it changes
  const [sortingType, setSortingType] = useState(null);
  const { i18n } = useTranslation();

  useEffect(() => {
    document.dir = i18n.dir();
    document.documentElement.lang = i18n.language;

    document.title = i18n.t("document_title");
  }, [i18n, i18n.language]);
  return (
    // sorting type needs to be accessed by both NavBar and DisplayAllGameCards components
    <AppContext.Provider value={{ sortingType, setSortingType }}>
      <div className="root-layout">
        <NavBar />
        <main>
          <Outlet />
        </main>
        <Footer />
      </div>
    </AppContext.Provider>
  );
}
