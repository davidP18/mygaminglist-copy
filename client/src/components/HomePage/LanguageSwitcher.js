import React from "react";
import "../styles/LanguageSwitcher.css";
import { useTranslation } from "react-i18next";
import { supportedLanguages } from "../../config/i18n";
import { languageCodeOnly } from "../../services/i18n";

function LanguageSwitcher() {
  const { i18n } = useTranslation();

  return (
    <div className="select">
      <select
        id="languageSwitcherMenu"
        value={languageCodeOnly(i18n.language)}
        onChange={e => i18n.changeLanguage(e.target.value)}
      >
        {supportedLanguages.map((lang) => (
          <option key={lang.code} value={lang.code}>
            {lang.name}
          </option>
        ))}
      </select>
    </div>
  );
}

export default LanguageSwitcher;
