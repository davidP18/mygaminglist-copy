import "../styles/Footer.css";
import logo from "../../assets/MyGamingList_Logo.png";
import { useTranslation } from "react-i18next";
/**
 * Display footer with the name of authors and link to the dataset
 * @author Dan Willis
 * @returns footer
 */
function Footer() {
  const { t } = useTranslation();
  return (
    <div id="footer">
      <div id="top">
        <div className="grid-item">
          <img id="logo-footer" src={logo} alt="logo" />
          <p id="slogan">{t("footer_game_slogan")}</p>
        </div>
        <div className="grid-item">
          <span className="authors">
            <p className="author" id="authors-title">
              {t("footer_authors")}
            </p>
            <p className="author">Dan Willis</p>
            <p className="author">Shiv Patel</p>
          </span>
        </div>
        <div className="grid-item">
          <p className="authors">
            David Pizzolongo <br />
            Christopher Dagenais <br />
            Edris Zoghlami
          </p>
        </div>
      </div>
      <hr />
      <div id="bottom">
        <p id="dataset">
          {t("dataset_from")}{" "}
          <a href="https://www.kaggle.com/datasets/deepann/80000-steam-games-dataset">
            Kaggle
          </a>
        </p>
        <p id="copyright">&copy; {t("copyright")}</p>
      </div>
    </div>
  );
}

export default Footer;
