/* eslint-disable no-undef */
import React, { useEffect, useRef, useState } from "react";
import jwt_decode from "jwt-decode";
import logo from "../../assets/MyGamingList_Logo.png";
import "../styles/NavBar.css";
import { Link, useLocation } from "react-router-dom";
import SearchBar from "./SearchBar";
import SortForm from "../SortedGamesPage/SortForm";
import LanguageSwitcher from "./LanguageSwitcher";
import { useTranslation } from "react-i18next";
import GameFilterComponent from "../CategorizedGamesPage/GameFilterComponent";
/**
 * Display navigation bar with search bar and navigation menu
 * @author Dan Willis
 * @returns navigation bar
 */
function NavBar() {
  const [user, setUser] = useState({});
  const { t } = useTranslation();
  const signInDivRef = useRef();
  const loginData = localStorage.getItem("loginData");

  // changes when you navigate to different pages
  const url = useLocation();

  useEffect(() => {
    //This function adjusts the image of the current logged in user to match the db and then sets the user
    async function setCurrentUser(userObject) {
      let obj = JSON.parse(userObject);
      try {
        const resp = await fetch(`/users/${obj.email}`);
        let json = await resp.json();
        obj.picture = json.pfp;
        setUser(obj);
      } catch {
        console.log("Failed to set the current logged in user");
      }
    }
    if (loginData != null) {
      setCurrentUser(loginData);
    } else {
      try {
        google.accounts.id.initialize({
          client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
          callback: handleCallbackResponse,
        });

        google.accounts.id.renderButton(signInDivRef.current, {
          theme: "outline",
          size: "large",
        });
      } catch {
        console.log("Failed to load Google login!");
      }
    }
  }, [loginData]);

  async function handleCallbackResponse(response) {
    const userObject = jwt_decode(response.credential);

    // The following block of code is used to check if a user object exists based on login
    let formData = new FormData();
    formData.append(
      "username",
      userObject.given_name + " " + userObject.family_name
    );
    formData.append("profile", userObject.picture);
    formData.append("email", userObject.email);
    const resp = await fetch("/users/login", {
      body: formData,
      method: "post",
    });
    let json = await resp.json();
    userObject.picture = json.profile;
    setUser(userObject);
    signInDivRef.current.hidden = true;
    localStorage.setItem("loginData", JSON.stringify(userObject));

    // force re-render of page (if on game or user list page)
    const currentPath = window.location.pathname;
    if (currentPath.startsWith("/game") || currentPath === "/users") {
      window.location.reload();
    }
  }

  function handleSignOut(event) {
    event.preventDefault();
    //remove the user and clear local storage
    setUser({});
    localStorage.removeItem("loginData");
    signInDivRef.current.hidden = false;
    // let user know that they will be redirected to the Home Page
    alert("Redirecting to Home...");
    window.location = "/";
  }

  return (
    <header className="header">
      <div id="topnav">
        <a id="logoLink" href="/">
          <img id="logo" src={logo} alt="logo" />
        </a>
        <SearchBar
          placeholder={
            url.pathname.includes("users") || url.pathname.includes("profile")
              ? t("search_user_text")
              : t("search_game_text")
          }
        />

        {/* google authentication */}
        <div id="signInDiv" ref={signInDivRef}></div>
        {/* if user is logged in, show sign out button */}
        {Object.keys(user).length !== 0 && (
          <button id="signout" onClick={handleSignOut}>
            {t("sign_out")}
          </button>
        )}
        {localStorage.getItem("loginData") && (
          <div>
            <Link to={`/profile`} state={{ user }}>
              <img id="profile-pic" src={user.picture} alt="profile" />
            </Link>
          </div>
        )}
      </div>

      {/* navigation */}
      <div id="bottomnav">
        <div id="navlinks">
          <div id="homeLinkDiv">
            <Link to="/">{t("home_button_text")} </Link>
          </div>
          <div id="bookmarkLinkDiv">
            <Link to="/users">{t("users_button_text")}</Link>
          </div>
          <div id="categoryButtonDiv">
            {(url.pathname === "/" || url.pathname.includes("category")) && (
              <GameFilterComponent />
            )}
          </div>
          {/* only show Sort button and dropdown menu if we are on the Home Page */}
          <div id="sortButtonDiv">
            {url.pathname === "/" && (
              <div tabIndex="0" id="dropdown">
                <button id="sort-link">
                  {t("sort_button_text")} &#9662;
                </button>
                <SortForm />
              </div>
            )}
          </div>
        </div>

        {/* filters */}
        <div id="languageChangerDiv">
          <LanguageSwitcher />
        </div>
      </div>
    </header>
  );
}

export default NavBar;
