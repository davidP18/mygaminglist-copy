import GameCard from "../GameCard.js";
import Pagination from "../Pagination.js";
import { AppContext } from "./RootComponent.js";
import "../styles/DisplayAllGameCards.css";
import { useContext, useEffect, useState } from "react";

/**
 * Display all games. Show 120 games at the time and use pagination to load more games.
 * @author Dan Willis
 * @returns list of games with pagination at the bottom
 */
function DisplayAllGameCards() {
  const GAME_LIMIT = 120;
  const [games, setGames] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  const { sortingType } = useContext(AppContext);

  useEffect(() => {
    // get the count of all games in db to calculate pagination
    async function fetchTotalPages() {
      try {
        let response = await fetch(`/games/count`);
        let result;
        if (response.ok) {
          result = await response.json();
          setTotalPages(result.count);
        }
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchTotalPages();
  }, []);

  // Fetches new subset of games every time the user changes page.
  // If the user clicks on the Sort button, the games will be sorted in the backend before 
  // being returned as a JSON response.
  useEffect(() => {
    let index = (currentPage - 1) * GAME_LIMIT;
    async function fetchData() {
      try {
        let url = `/games/${index}`;
        // sorting type starts as null, until the user chooses a sort option like title
        if (sortingType) {
          url += `?sort=${sortingType}`;
        }

        const response = await fetch(url);
        if (response.ok) {
          const result = await response.json();
          setGames(result); // set updated games
        }
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchData();
  }, [currentPage, sortingType]);

  window.scrollTo(0, 0);

  return (
    <div>
      <div id="gameContainer">
        {games.map((game) => (
          <GameCard key={game._id} game={game} />
        ))}
      </div>
      <div id="games-pagination">
        <Pagination
          currentPage={currentPage}
          total={totalPages}
          limit={GAME_LIMIT}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </div>
    </div>
  );
}

export default DisplayAllGameCards;
