let navBar = null;

beforeAll(() => {
    // create nav bar without google auth
    navBar = document.createElement('header');

    // div with navigation links
    const linksDiv = document.createElement('div');
    const link = document.createElement('a');
    link.textContent = 'Home';
    linksDiv.append(link);

    // filter drop-down menu
    const filterMenu = document.createElement('select');
    const filterOption = document.createElement('option');
    filterOption.textContent = 'rating';
    filterMenu.append(filterOption);

    navBar.append(linksDiv, filterMenu);
    document.body.append(navBar);
});

test('renders header with nav elements and drop-down filter menu', () => {
    expect(navBar.childNodes.length).toBe(2);
    expect(navBar.childNodes[0].nodeName).toBe('DIV');
    expect(navBar.childNodes[1].nodeName).toBe('SELECT');
});
