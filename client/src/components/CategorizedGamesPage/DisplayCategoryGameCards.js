/* eslint-disable react-hooks/exhaustive-deps */
import GameCard from "../GameCard.js";
import Pagination from "../Pagination.js";
import "../styles/DisplayAllGameCards.css";
import { useEffect, useMemo, useState } from "react";
import { useLocation } from "react-router-dom";

/**
 * Display filtered games. Show 120 games at the time and use pagination to load more games.
 * @author Edris Zoghlami Dan Willis David Pizzolongo
 * @returns list of games with pagination at the bottom
 */
function DisplayCategoryGameCards() {
  const GAME_LIMIT = 120;
  // get chosen categories and relative url from the current Location object 
  let { state: categoryProps, pathname } = useLocation();
  const [games, setGames] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);

  // only initialize categories variable on first render or when a user selects other categories
  const categories = useMemo(() => {
    // categories is null if the user navigated to the page manually using the address bar (did not submit a form) 
    if (!categoryProps) {
      // get category string from url and format it to conform to the server's API format
      const checkedCategories = pathname.replace("/category/", "").replaceAll("-", "/");
      return checkedCategories.replace(/\/+$/, ""); //remove trailing slashes (if any)
    }
    return categoryProps;
  }, [categoryProps]);

  // get the count of all games in db to calculate pagination
  async function fetchTotalPages() {
    try {
      const response = await fetch(
        `/games/filter/category/count/${categories}`
      );
      const result = await response.json();
      setCurrentPage(1);    // reset current page to 1
      setTotalPages(result.count);
    } catch (error) {
      console.error(error.message);
      setTotalPages(0);
    }
  }

  // Fetch all games from the current index and update the state. 
  async function fetchData() {
    const index = (currentPage - 1) * GAME_LIMIT;
    try {
      const response = await fetch(
        `/games/filter/category/index/${index}/${categories}`
      );
      const result = await response.json();
      setGames(result);
    } catch (error) {
      console.error(error.message);
      setGames([]);
    }
  }

  useEffect(() => {
    fetchTotalPages();
    fetchData();
  }, [categories]);  // only fetch if user chose different categories

  // Fetches new subset of games every time the user changes page.
  useEffect(() => {
    fetchData();
  }, [currentPage]);

  window.scrollTo(0, 0);

  return (
    <div>
      {totalPages === 0 && (
        <p className="error-msg">No games available. Please reduce the search criteria.</p>
      )}
      <div id="gameContainer">
        {games.map((game) => (
          <GameCard key={game._id} game={game} />
        ))}
      </div>
      <div id="games-pagination">
        <Pagination
          currentPage={currentPage}
          total={totalPages}
          limit={GAME_LIMIT}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </div>
    </div>
  );
}

export default DisplayCategoryGameCards;
