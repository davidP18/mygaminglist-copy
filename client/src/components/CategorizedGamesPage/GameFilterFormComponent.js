import { React, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/GameFilterFormComponent.css";
import { useTranslation } from "react-i18next";
/**
 * Display the filter category form when the category button is pressed
 * @author Edris Zoghlami
 * @returns the filter category form
 */

// all the categories
const categories = [
  "Action",
  "Adventure",
  "Free",
  "Multiplayer",
  "Singleplayer",

  "Sports",
  "Horror",
  "Shooter",
  "Online",
  "Competitive",

  "Classic",
  "Local",
  "Violent",
  "Fantasy",
  "Arcade",
];

function GameFilterFormComponent(removeFilterForm) {
  const navigate = useNavigate();
  const [checkedCategories, setCheckedCategories] = useState([]);
  const { t } = useTranslation();

  // add or remove a category from the checked category list
  const addRemoveCategory = (category) => {
    const categoryIndex = checkedCategories.indexOf(category);
    // category was removed by the user so remove it from state
    if (categoryIndex >= 0) {
      const newCategories = checkedCategories.filter((category, index) => index !== categoryIndex);
      setCheckedCategories(newCategories);
    }
    else {
      // add new category to the end of the array
      setCheckedCategories(prevCategories => [...prevCategories, category]);
    }
  };

  // handles the submit. Will take only the checked categories and display them
  const handleSubmit = (event) => {
    event.preventDefault();
    let categoryString = "";
    for (const checkedCategory of checkedCategories) {
      categoryString += "-" + checkedCategory;
    }
    // remove trailing dash
    categoryString = categoryString.slice(1);

    if (categoryString) {
      // replace dashes with slashes to follow api format for fetching categorized games
      navigate(`/category/${categoryString}`, { state: categoryString.replaceAll("-", "/") });
    } else {
      navigate(`/`);
    }

    removeFilterForm.removeFilterForm();
    setCheckedCategories([]);
  };

  return (
    <div id="game-filter-form-component">
      <form id="filter-form" onSubmit={handleSubmit}>
        <div className="subcategorydiv">
          {categories.map((category, index) => {
            return (
              <label key={index} htmlFor={category}>
                <input
                  type="checkbox"
                  id={category}
                  name={category}
                  onChange={() => addRemoveCategory(category)}
                />
                {category}
              </label>
            );
          })}
        </div>
        <br />
        <input
          type="submit"
          id="filterSubmitButton"
          value={t("filter")}
        ></input>
      </form>
    </div>
  );
}

export default GameFilterFormComponent;
