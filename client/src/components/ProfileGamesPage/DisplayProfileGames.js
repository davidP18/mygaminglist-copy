import "../styles/DisplayAllGameCards.css"
import "../styles/DisplayProfileGames.css";
import GameCard from '../GameCard.js'
import { useLocation } from "react-router-dom"
import { useEffect, useState } from "react";

/**
 * Display user's selected list of games which are bookmarks, playing and completed
 * @author Dan Willis
 * @returns list of games
 */
function DisplayProfileGames() {
  const location = useLocation();
  const games = location.state?.games;
  const [profileGames, setprofileGames] = useState([])

  useEffect(() => {
    // get all the games based on game_id
    async function setAllGames() {
      let profile_games = await Promise.all(games.map(async (item) => {
        try {
          let resp = await fetch(`/games/id/${item}`)
          let json = await resp.json()
          return json.game
        } catch (error) {
          console.error(error.message)
        }
      }))
      setprofileGames(profile_games)
    }
    if (games == null) return;
    setAllGames();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div id='gameContainer'>
      {games == null && (
        <p className="error-msg">
          Failed to find catalogued games! Return to {" "}
          <a href="/users">Users</a>
          {" "} or {" "}
          <a href="/profile">Profile</a>
        </p>
      )}
      {games?.length === 0 && (
        <div id='noGames'>
          No Games in this collection
        </div>
      )}
      {profileGames.map(game => <GameCard key={game._id} game={game} />)}
    </div>
  )
}

export default DisplayProfileGames