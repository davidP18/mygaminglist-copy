import { React, useState, useContext } from "react";
import "../styles/SortForm.css";
import { useTranslation } from "react-i18next";
import { AppContext } from "../HomePage/RootComponent";
/**
 * The SortForm component handles the logic of managing the form to sort games.
 * When you choose a sort option and click on the Sort button, the
 * setSortingType function will be called to modify the App's context
 * and notify the DisplayAllGameCards component to show the newly sorted games.
 * @author David Pizzolongo
 * @returns radio button form with Sort button
 */
function SortForm() {
  // keeps track of user input in the form
  const [oldInput, setOldInput] = useState(null);
  const { setSortingType } = useContext(AppContext);
  const { t } = useTranslation();

  function handleSubmit(e) {
    e.preventDefault();
    // get checked input
    const input = document.querySelector('#dropdown-form input[type="radio"]:checked');
    const userInput = input.value;

    // only update state when input changes from the last sort
    if (userInput !== oldInput) {
      setOldInput(userInput);
      // update DisplayAllGameCards component
      setSortingType(userInput);
    }
  }

  return (
    <form id="dropdown-form" onSubmit={handleSubmit}>
      <input
        type="radio"
        id="rating"
        name="sort-fields"
        value="likesDesc"
        className="dropdown-input"
        required
      />
      <label htmlFor="rating" className="dropdown-labels">
        {t("rating")}
      </label>
      <br></br>
      <input
        type="radio"
        id="title-asc"
        name="sort-fields"
        value="nameAsc"
        className="dropdown-input"
      />
      <label htmlFor="title-asc" className="dropdown-labels" >
        {t("title_ASC")}
      </label >
      <br></br>
      <input
        type="radio"
        id="title-desc"
        name="sort-fields"
        value="nameDesc"
        className="dropdown-input"
      />
      <label htmlFor="title-desc" className="dropdown-labels" >
        {t("title_DESC")}
      </label >
      <br></br>

      <input type="submit" value={t("sort_button_text")} id="submit-btn" />
    </form >
  );
}

export default SortForm;
