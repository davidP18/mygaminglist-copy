/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { getFormData, postToDB } from '../../utils/utils';

// This component manages the bookmark, playing and completed collections of a user
// and updates the database when the user clicks on any of these catalogue buttons. 
// Since it is memoized, it will only be rendered if props change (avoids unnecessary re-renders
// when the Snackbar appears).
const CatalogueButtons = React.memo(({ gameId, userCatalogueState, showSnackbar }) => {
    // determines if component has not been fully loaded yet 
    const [isLoading, setIsLoading] = useState(true);
    const [isBookmarked, setIsBookmarked] = useState(false);
    const [isPlaying, setIsPlaying] = useState(false);
    const [isCompleted, setIsCompleted] = useState(false);
    const { t } = useTranslation();

    // update the state of the buttons when a user navigates to a new game
    useEffect(() => {
        (async () => {
            setIsLoading(true);

            const catalogueState = await userCatalogueState;
            setIsBookmarked(catalogueState.isBookmarked);
            setIsPlaying(catalogueState.isPlaying);
            setIsCompleted(catalogueState.isCompleted);

            setIsLoading(false);
        })();
    }, [gameId]);

    // This function is called any time the user clicks any of the cataloguing buttons
    // If the user is signed in, the game will be stored/removed from their collection in the database
    // and the styling of the catalogue button will reflect this change. If the user is not signed
    // in, a Snackbar alert will be shown stating that they must be signed in to perform these actions.
    function catalogueGame(catalogueButton) {
        let userObject = JSON.parse(localStorage.getItem("loginData"));
        // user is signed in 
        if (userObject) {
            const catalogueType = catalogueButton.id.replace("BTN", "");
            const formData = getFormData(userObject.email, catalogueType);
            try {
                postToDB(formData, gameId, catalogueType);
                toggleCatalogueButton(catalogueType);
            } catch {
                console.log("Network error!");
            }
        } else {
            showSnackbar(true);
        }
    }

    // Apply new styling based on what catalogue action the user has performed.
    function toggleCatalogueButton(catalogueType) {
        // if the playing button was selected then the completed button should be unselected (and vice-versa)
        if (catalogueType === "playing") {
            // toggle playing button
            setIsPlaying(prevValue => !prevValue);
            setIsCompleted(false);
        }
        else if (catalogueType === "completed") {
            setIsPlaying(false);
            // toggle completed button
            setIsCompleted(prevValue => !prevValue);
        }
        else {
            // toggle bookmark button 
            setIsBookmarked(prevValue => !prevValue);
        }
    }

    // render buttons only when they are all properly styled and the state of the component
    // reflects the state of the db
    if (!isLoading) {
        return (
            <div id="user-catalogue-div">
                <label
                    className={"bookmarkBTNLabel " + (isBookmarked ? "checked" : "")}
                    id="bookmarkLabel"
                    htmlFor="bookmarkBTN"
                    title="Bookmark"
                >
                    <input
                        type="checkbox"
                        name="input-option"
                        id="bookmarkBTN"
                        className="preferenceInput"
                        onClick={e => catalogueGame(e.target)}
                    />
                    {t("bookmark")}
                </label>

                <label
                    className={"catalogueBTNLabel " + (isPlaying ? "checked" : "")}
                    id="playingLabel"
                    htmlFor="playingBTN"
                    title="Playing"
                >
                    <input
                        type="radio"
                        name="input-option"
                        id="playingBTN"
                        className="preferenceInput"
                        onClick={e => catalogueGame(e.target)}
                    />
                    {t("playing")}
                </label>

                <label
                    className={"catalogueBTNLabel " + (isCompleted ? "checked" : "")}
                    id="completedLabel"
                    htmlFor="completedBTN"
                    title="Completed"
                >
                    <input
                        type="radio"
                        name="input-option"
                        id="completedBTN"
                        className="preferenceInput"
                        onClick={e => catalogueGame(e.target)}
                    />
                    {t("completed")}
                </label >
            </div >
        )
    }
    return null;
});

export default CatalogueButtons;