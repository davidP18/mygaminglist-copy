import React from "react";
import "../styles/DisplayGameDetails.css";

import GameInfoComponent from "./GameInfoComponent";
import GamePreferenceComponent from "./GamePreferenceComponent";
import SubmitCommentForm from "./SubmitCommentForm";

import DisplayAllComments from "./DisplayAllComments";
import { useLocation } from "react-router-dom";

/**
 * The DisplayGameDetails component takes as input a state object from the Link that calls it. The object contains all the
 * necessary information about the chosen game's details and the comments relating to that game. It displays them
 * all using many divs and seperate external components and returns 1 individual-game-page div representing them all.
 * @author Edris Zoghlami
 * @returns DisplayGameDetails component containing a individual-game-page
 */
function DisplayGameDetails() {
  let { state } = useLocation();
  const isValidGame = state != null;
  window.scrollTo(0, 0);

  if (!isValidGame) {
    return (
      <p className="error-msg">
        &#10060; Game does not exist. Return to <a href="/">Home</a> &#10060;
      </p>
    );
  }
  return (
    <div id="individual-game-page">
      <div id="full-game-details">
        <div id="top-div">
          <div id="game-title-div">
            <div id="game-title">{state.game.name}</div>
          </div>
        </div>

        <div id="middle-div">
          <div id="game-image-div">
            <a href={state.game.steam_url.length > 0 && state.game.steam_url} title={state.game.steam_url && "View on Steam!"} target="_blank" rel="noreferrer">
              <img
                id="game-image"
                src={state.game.img_url}
                alt="Game header from Steam"
              />
            </a>
          </div>

          <GameInfoComponent
            date={state.game.date}
            developer={state.game.developer}
            publisher={state.game.publisher}
            tags={state.game.tags}
          />
        </div>

        <div id="bottom-div">
          <GamePreferenceComponent
            id={state.game._id}
          />

          <div id="game-description">{state.game.desc.replace('About This Game', '')}</div>
        </div>
      </div>
      <SubmitCommentForm gameId={state.game._id} />

      <DisplayAllComments comments={state.game.comments} />
    </div>
  );
}

export default DisplayGameDetails;
