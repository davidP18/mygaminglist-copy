import React from "react";
import { useEffect, useState } from "react";
import "../styles/DisplayComment.css";
import { Link } from "react-router-dom";
/**
 * The DisplayComment component takes as input object representing comments and the user that posted them
 * The data is them displayed using divs and images for the user profile image
 * @author Edris and David
 * @param props contains data like the comment user, the user profile picture and the comment content
 * @returns comment div
 */
function DisplayComment({ userId, comment }) {
  const [user, setUser] = useState({});
  const [isValidUser, setIsValidUser] = useState(true);

  // useEffect will set the user to be the one that's associated to the comment
  useEffect(() => {
    async function fetchUser() {
      try {
        let response = await fetch(`/users/id/${userId}`);
        let result;

        if (response.ok) {
          result = await response.json();
          // response was null because there is no user with that id anymore
          if (result == null) {
            setIsValidUser(false);
            return;
          }
          setUser(result);
        }
      } catch (e) {
        console.error(e.message);
      }
    }
    fetchUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="comment">
      <div className="comment-content">
        <div className="image-div">
          {/* only show profile pic if user exists */}
          {isValidUser && (
            <Link to={`/users/profile/${userId}`} state={{ user }}>
              <img className="user-image" src={user.pfp} alt="" />
            </Link>)}
        </div>

        <div className="name-text-comment">
          <div className="username">{isValidUser ? user.username : 'deleted-user'}</div>

          <div className="comment-text">{comment}</div>
        </div>

      </div>
    </div>
  );

}

export default DisplayComment;
