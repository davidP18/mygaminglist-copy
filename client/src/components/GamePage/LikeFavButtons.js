/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import likeButtonSrc from "../../assets/likebutton.png";
import favoriteButtonSrc from "../../assets/favoritebutton.png";
import { useTranslation } from 'react-i18next';
import { getFormData, postToDB } from '../../utils/utils';

/**
 * This memoized component handles the functionality for liking and favoriting games,
 * and renders the appropriate HTML elements. However, if the like count of a game does not match 
 * the like count stored in the database, the component will display a Loading message and wait
 * until the state of the component has been correctly updated.
 * @author David
 */
const LikeFavButtons = React.memo(({ gameId, userCatalogueState, showSnackbar }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [likeCount, setLikeCount] = useState(0);
    const [isLiked, setIsLiked] = useState(false);
    const [isFavorite, setIsFavorite] = useState(false);
    const userObject = JSON.parse(localStorage.getItem("loginData"));
    const { t } = useTranslation();

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const catalogueState = await userCatalogueState;
            // set state for like and fav buttons and update their styling
            setIsLiked(catalogueState.isLiked);
            setIsFavorite(catalogueState.isFav);

            // fetch up-to-date like count from database
            try {
                const response = await fetch("/games/id/" + gameId);
                if (response.ok) {
                    const gameObj = await response.json();
                    setLikeCount(gameObj.game.likes);
                }
            } catch (error) {
                console.error(error.message);
            }
            setIsLoading(false);
        })();
    }, [gameId]);

    // Update like button styling and the like count, depending on if the user liked or  
    // unliked a game. This function is invoked after the likes have been updated in the database.
    function toggleLike() {
        const isLikedNew = !isLiked;
        // add or remove class "liked" from the like button
        setIsLiked(isLikedNew);
        // update like count depending on if game was liked or unliked
        if (isLikedNew) {
            setLikeCount(prevCount => ++prevCount);
        }
        else {
            setLikeCount(prevCount => --prevCount);
        }
    }

    // This function is called any time the user clicks the like button
    function likeGame() {
        if (userObject) {
            const formData = getFormData(userObject.email, "like");
            try {
                // post like to database
                postToDB(formData, gameId, "like");
                toggleLike();
            } catch {
                console.log("Network error, failed to post like!");
            }
        } else {
            showSnackbar(true);
        }
    }

    // This function is called any time the user clicks the favorite game button
    function setFavGame() {
        if (userObject) {
            const formData = getFormData(userObject.email, "favorite");
            try {
                // change fav game in db
                postToDB(formData, gameId, "favorite");
                // apply new styling on favorite button 
                setIsFavorite(prevValue => !prevValue);
            } catch {
                console.log("Network error!, failed to make game favorite!");
            }
        } else {
            showSnackbar(true);
        }
    }

    // render buttons and like count only once the state of the component matches the state of the db
    if (!isLoading) {
        return (
            <div id="game-score-div">
                <div id="game-score-sub-div">
                    <div id="game-score">
                        {t("score_text")}
                        {likeCount}
                    </div>
                </div>
                <div id="like-game-div">
                    <img
                        id="like-button"
                        src={likeButtonSrc}
                        onClick={likeGame}
                        alt="likebutton"
                        title="Like"
                        className={isLiked ? "liked" : ""}
                    ></img>
                </div>

                <div id="favorite-game-div">
                    <img
                        id="favorite-button"
                        src={favoriteButtonSrc}
                        onClick={setFavGame}
                        alt="likebutton"
                        title="Favorite"
                        className={isFavorite ? "fav" : ""}
                    ></img>
                </div>
            </div>
        )
    }
    return <p className='loading-msg'>Loading...</p>;
});

export default LikeFavButtons;