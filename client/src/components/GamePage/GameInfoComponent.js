import React from "react";
import "../styles/GameInfoComponent.css";
import { useTranslation } from "react-i18next";

/**
 * The GameInfoComponent takes as input a props containing all the necessary information
 * about the chosen game's information relating to production. It displays them all using many divs and
 * returns 1 game-info div representing them all.
 * @author Edris Zoghlami
 * @param game contains data like release date, developer, publisher and categories
 * @returns GameInfoComponent div
 */
function GameInfoComponent(game) {
  const { t } = useTranslation();
  return (
    <div id="game-info">
      <div id="release-date-div">
        <div className="info-subtitle">{t("release_date_text")}</div>
        <div className="info-content">{game.date}</div>
      </div>

      <div id="developer-div">
        <div className="info-subtitle">{t("developer_text")}</div>
        <div className="info-content">{game.developer}</div>
      </div>

      <div id="publisher-div">
        <div className="info-subtitle">{t("publisher_text")}</div>
        <div className="info-content">{game.publisher}</div>
      </div>

      <div id="categories-div">
        <div className="info-subtitle">{t("categories_text")}</div>
        <div className="info-content">
          {game.tags.map((tag, index) => (
            <span key={index}>{tag}, </span>
          ))}
        </div>
      </div>
    </div>
  );
}

export default GameInfoComponent;
