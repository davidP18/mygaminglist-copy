/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useMemo } from "react";
import { Snackbar } from "@mui/material";
import "../styles/GamePreferenceComponent.css";
import CatalogueButtons from "./CatalogueButtons";
import LikeFavButtons from "./LikeFavButtons";

/**
 * This high-level component contains functions to determine what games are currently registered
 * in the user's various collections (bookmarks, playing, completed, etc.). This result 
 * is used by child components to show visually what the user has catalogued thus far and 
 * handle any modifications the user plans on making (if any). 
 * @author Edris and David
 * @param props contains data such as game id and like count
 * @returns GamePreferenceComponent div
 */
function GamePreferenceComponent({ id }) {
  const [logingAlert, setLogingAlert] = useState(false);

  // This function fetches the current user, using their email from 
  // Local Storage, and returns the json object which contains all the ids of games
  // that are stored in their collection.  
  async function getCurrentUser() {
    const userObject = JSON.parse(localStorage.getItem("loginData"));
    if (!userObject) return;

    let currentUser;
    try {
      const resp = await fetch(`/users/${userObject.email}`);
      currentUser = await resp.json();
      return currentUser;
    } catch {
      console.log("Failed to fetch user");
      return;
    }
  }

  // Gets the new catalogue state when a user navigates to another game. This function is only created and 
  // invoked when the game id changes, and its return value will be used by child components to update their own state.
  const userCatalogueState = useMemo(() => {
    // This function iterates over an array of catalogue types (ex: bookmarks) and determines if a given game is stored
    // in any of those collections under the current user's account. 
    async function getUserCatalogueState(catalogueTypes, game) {
      const currentUser = await getCurrentUser();
      const newState = { isLiked: false, isFav: false, isBookmarked: false, isPlaying: false, isCompleted: false };
      if (!currentUser) return newState;

      for (const catalogueType of catalogueTypes) {
        if (currentUser[catalogueType]?.includes(game)) {
          switch (catalogueType) {
            case "likes":
              newState.isLiked = true;
              break;
            case "favorite":
              newState.isFav = true;
              break;
            case "playing":
              newState.isPlaying = true;
              break;
            case "completed":
              newState.isCompleted = true;
              break;
            case "bookmarks":
              newState.isBookmarked = true;
              break;
            default:
              break;
          }
        }
      }
      return newState;
    }

    return getUserCatalogueState(["likes", "favorite", "bookmarks", "playing", "completed"], id);
  }, [id]);


  return (
    <div id="classify-game-div">
      <LikeFavButtons gameId={id} userCatalogueState={userCatalogueState} showSnackbar={setLogingAlert} />

      <CatalogueButtons gameId={id} userCatalogueState={userCatalogueState} showSnackbar={setLogingAlert} />

      <Snackbar
        message="You must be logged in to use this feature"
        autoHideDuration={4000}
        open={logingAlert}
        onClose={() => setLogingAlert(false)}
      />
    </div >
  );
}

export default GamePreferenceComponent;
