import React from "react";
import { useState } from "react";
import "../styles/SubmitCommentForm.css";
import { Snackbar } from "@mui/material";
import arrow from "../../assets/arrow.png";
import { useTranslation } from "react-i18next";

/**
 * This component allows a user to send comments for a specific game
 * @author Edris Zoghlami
 * @param gameId 
 * @returns form for sending comments
 */
function SubmitCommentForm({ gameId }) {
  const [comment, setComment] = useState("");
  const [logingAlert, setLogingAlert] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const { t } = useTranslation();

  function commentHandler(e) {
    setComment(e.target.value);
  }

  // Helper function that details what to do on submit
  async function handleSubmit(e) {
    e.preventDefault();
    let userObject = JSON.parse(localStorage.getItem("loginData"));
    if (userObject) {
      let formData = new FormData();
      formData.append("email", userObject.email);
      formData.append("comment", comment);

      const resp = await fetch(`/games/${gameId}/comment`, {
        body: formData,
        method: "post",
      });
      if (resp.ok) {
        setIsSubmitted(true);
        setLogingAlert(true);
      }
      else {
        alert("Network error: Failed to post comment!")
      }
    } else {
      setIsSubmitted(false);
      setLogingAlert(true);
    }
    e.target.reset();
  }

  return (
    <>
      <div id="submit-comment-container">
        <div id="submit-comment-component">
          <h2 id="leave-comment-h2">{t("submit_comment_text")}</h2>

          <form id="comment-form" onSubmit={handleSubmit}>
            <textarea
              form="comment-form"
              id="comment-textarea"
              name="comment"
              placeholder={t("comment_form_placeholder")}
              rows="3"
              onChange={commentHandler}
              required
            ></textarea>

            <button id="submit-comment-btn">
              <img id="send-img" src={arrow} alt="Send" />
            </button>
          </form>

          <Snackbar
            message={isSubmitted ? "Comment posted!" : "You must be logged in to submit a comment"}
            autoHideDuration={4000}
            open={logingAlert}
            onClose={() => setLogingAlert(false)}
          />
        </div>
      </div>
    </>
  );
}

export default SubmitCommentForm;
